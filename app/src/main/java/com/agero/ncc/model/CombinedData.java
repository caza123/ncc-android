package com.agero.ncc.model;

import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccMotionData;

public class CombinedData {
    private NccLocationData lastLocationData;
    private NccMotionData lastMotionData;

    public CombinedData(NccLocationData lastLocationData, NccMotionData lastMotionData) {
        this.lastLocationData = lastLocationData;
        this.lastMotionData = lastMotionData;
    }

    public NccLocationData getLastLocationData() {
        return lastLocationData;
    }

    public void setLastLocationData(NccLocationData lastLocationData) {
        this.lastLocationData = lastLocationData;
    }

    public NccMotionData getLastMotionData() {
        return lastMotionData;
    }

    public void setLastMotionData(NccMotionData lastMotionData) {
        this.lastMotionData = lastMotionData;
    }

}
