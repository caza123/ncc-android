package com.agero.ncc.model

import android.graphics.Bitmap

/**
 * Data model for a user profile
 */
class UserProfile {

    var name: String? = null

    var email: String? = null

    var avatar: Bitmap? = null
}
