package com.agero.ncc.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;

import com.agero.ncc.R;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.fragments.CreateAccountFragment;
import com.agero.ncc.fragments.PasswordFragment;
import com.agero.ncc.fragments.WelcomeSignInFragment;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import timber.log.Timber;

/**
 * Created by sdevabhaktuni on 12/8/17.
 */

public class WelcomeActivity extends BaseActivity {


    @Inject
    SharedPreferences mPrefs;
    private String invitationCode;
    private UserError mUserError;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NCCApplication.getContext().getComponent().inject(this);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        mUserError = new UserError();
        if (getIntent().hasExtra(NccConstants.IS_REFRESH_TOKEN_FAILED)) {
            pushFragment(PasswordFragment.newInstance(""));
        } else {
            initBranch();
        }
    }

    private void initBranch() {
        if (Utils.isNetworkAvailable()) {
            //Check the google play service whether it is updated
            if (Utils.isGooglePlayServicesAvailable(this)) {
                // Branch init
                Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
                    @Override
                    public void onInitFinished(JSONObject referringParams, BranchError error) {
                        if (error == null) {
                            Timber.i(referringParams.toString());
                            JSONObject customData = referringParams.optJSONObject("custom_data");
                            if (customData != null) {
                                invitationCode = customData.optString("invitation_code");
                                Timber.d(invitationCode);
                            }

                            if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "[]")) && !TextUtils.isEmpty(mPrefs.getString(NccConstants.APIGEE_TOKEN, ""))) {
                               startHomeScreen();
                            } else {

                                if (!TextUtils.isEmpty(invitationCode)) {
                                    pushFragment(CreateAccountFragment.newInstance(invitationCode), getString(R.string.title_createaccount));
                                } else {
                                    pushFragment(WelcomeSignInFragment.newInstance(invitationCode));
                                }


                            }


                        } else {
                            pushFragment(WelcomeSignInFragment.newInstance(invitationCode));
                            Timber.i(error.getMessage());
                        }
                    }
                }, this.getIntent().getData(), this);
            }
        }else {
            if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ROLE, "[]")) && !TextUtils.isEmpty(mPrefs.getString(NccConstants.APIGEE_TOKEN, ""))) {
                startHomeScreen();
            } else {
                pushFragment(WelcomeSignInFragment.newInstance(invitationCode));
            }
        }
    }

    private void startHomeScreen() {
        Intent intent = new Intent(WelcomeActivity.this, HomeActivity.class);
        if (getIntent().hasExtra(NccConstants.NOTIFICATION_TYPE)) {
            intent.putExtra(NccConstants.NOTIFICATION_TYPE, getIntent().getStringExtra(NccConstants.NOTIFICATION_TYPE));
            intent.putExtra(NccConstants.NOTIFICATION_ENTITY_ID, getIntent().getStringExtra(NccConstants.NOTIFICATION_ENTITY_ID));
        }
        SparkLocationUpdateService.clearList();
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }


    public void pushFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        try {
            fragmentManager.beginTransaction().replace(R.id.frame_welcome_screen_container, fragment, fragment.getClass().getCanonicalName()).commit();
        } catch (IllegalStateException ille) {
            fragmentManager.beginTransaction().replace(R.id.frame_welcome_screen_container, fragment, fragment.getClass().getCanonicalName()).commitAllowingStateLoss();
        }

    }

    public void pushFragment(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();
        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_welcome_screen_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ille) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_welcome_screen_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }

        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_welcome_screen_container, fragment, tag).commit();
            } catch (IllegalStateException ille) {
                fragmentManager.beginTransaction().replace(R.id.frame_welcome_screen_container, fragment, tag).commitAllowingStateLoss();
            }
        }

    }

    public void mintlogEvent(String event) {
        HashMap<String, String> extraData = new HashMap<>();
        if (event.toUpperCase(Locale.ENGLISH).contains("FIREBASE") || event.toUpperCase(Locale.ENGLISH).contains("API")) {
            extraData.put("error", event);
            if (event.toUpperCase(Locale.ENGLISH).contains("FIREBASE")) {
                mintlogEventExtraData("Firebase Exception", extraData);
            } else {
                mintlogEventExtraData("Api Exception", extraData);
            }
        } else {
            mintlogEventExtraData(event, null);
        }

    }

    public void mintlogEventExtraData(String event, HashMap<String, String> extraDatas) {
        Mint.clearExtraData();
        if (extraDatas != null) {
            for (Map.Entry<String, String> extraData :
                    extraDatas.entrySet()) {
                Mint.addExtraData(extraData.getKey(), extraData.getValue());
            }
        }
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Mint.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
            Mint.addExtraData("user_id", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))) {
            Mint.addExtraData("facility_id", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        }

        Mint.logEvent(event, MintLogLevel.Debug);
        Mint.flush();
    }

    public void mintLogException(Exception ex) {
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Mint.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        if (!TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))) {
            Mint.addExtraData("FacilityId", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        }
        Mint.logException(ex);
        Mint.flush();
    }
}
