package com.agero.ncc.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.utils.NccConstants;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class JobStatusBottomSheetDialog extends BottomSheetDialogFragment {

    HomeActivity mHomeActivity;
    JobStatusDialogClickListener jobStatusDialogClickListener;

    public static JobStatusBottomSheetDialog getInstance() {
        return new JobStatusBottomSheetDialog();
    }


    public void setJobStatusDialogClickListener(JobStatusDialogClickListener jobStatusDialogClickListener) {
        this.jobStatusDialogClickListener = jobStatusDialogClickListener;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_job_status_bottom_sheet, container, false);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        return view;
    }

    @OnClick({R.id.text_label_enroute, R.id.text_label_onscene, R.id.text_label_towing, R.id.text_label_arrival, R.id.text_label_placehold})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_enroute:
                jobStatusDialogClickListener.onJobStatusChanged(NccConstants.JOB_STATUS_EN_ROUTE);
                break;
            case R.id.text_label_onscene:
                jobStatusDialogClickListener.onJobStatusChanged(NccConstants.JOB_STATUS_ON_SCENE);
                break;
            case R.id.text_label_towing:
                jobStatusDialogClickListener.onJobStatusChanged(NccConstants.JOB_STATUS_TOWING);
                break;
            case R.id.text_label_arrival:
                jobStatusDialogClickListener.onJobStatusChanged(NccConstants.JOB_STATUS_DESTINATION_ARRIVAL);
                break;
            case R.id.text_label_placehold:
                jobStatusDialogClickListener.onJobStatusChanged(NccConstants.JOB_STATUS_PLACE_HOLD);
                break;
        }
    }

    public interface JobStatusDialogClickListener {
        void onJobStatusChanged(String jobStatus);
    }

}
