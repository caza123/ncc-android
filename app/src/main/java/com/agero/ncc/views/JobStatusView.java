package com.agero.ncc.views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Custom view to re use in multiple getDisplayStatusText items in JobStatusFragment
 */
public class JobStatusView extends ConstraintLayout {
    @BindView(R.id.text_job_status)
    TextView mTextStatus;
    @BindView(R.id.text_job_status_description)
    TextView mTextStatusDescription;
    @BindView(R.id.image_job_selected)
    ImageView mImageSelected;

    public JobStatusView(Context context) {
        super(context);
        init();
    }

    public JobStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public JobStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.job_status_item, this);
        ButterKnife.bind(this, view);
    }


    public void setIsDisable(boolean isDisable) {
        if(isDisable) {
            mTextStatus.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
            mTextStatusDescription.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
            setEnabled(false);
            mImageSelected.setImageResource(R.drawable.ic_check_24px_grey);

        }else{
            mTextStatus.setTextColor(getResources().getColor(R.color.text_selector_job_status));
            mTextStatusDescription.setTextColor(getResources().getColor(R.color.text_selector_job_status));
            setEnabled(true);
            mImageSelected.setImageResource(R.drawable.ic_check_24px_blue);
        }
    }

    /**
     * To reset UI to original state.
     */
    public void resetViews() {
        setEnabled(false);
        mTextStatus.setEnabled(false);
        mImageSelected.setVisibility(GONE);
    }

    /**
     * To set getDisplayStatusText and description values.
     *
     * @param status            getDisplayStatusText.
     * @param statusDescription description.
     */
    public void setStatusAndDescription(String status, String statusDescription) {
        mTextStatus.setText(Utils.getDisplayStatusText(getContext(), status));
        mTextStatusDescription.setText(statusDescription);
    }

    /**
     * To enable getDisplayStatusText and description text views.
     *
     * @param isStatusEnabled boolean.
     */
    public void setStatusAndDescriptionEnabled(boolean isStatusEnabled, boolean isDescriptionVisible) {
        mTextStatus.setEnabled(isStatusEnabled);
        if (isDescriptionVisible) {
            mTextStatusDescription.setVisibility(VISIBLE);
        } else {
            mTextStatusDescription.setVisibility(GONE);
        }
    }

    /**
     * To show tick icon when getDisplayStatusText is selected.
     *
     * @param isSelected
     */
    public void setStatusSelected(boolean isSelected) {
        if (isSelected) {
            mImageSelected.setVisibility(VISIBLE);
        } else {
            mImageSelected.setVisibility(GONE);
        }
    }

    /**
     * To set and enable getDisplayStatusText text.
     *
     * @param status
     */
    public void setStatusText(String status) {
        mTextStatus.setEnabled(true);
        mTextStatus.setText(Utils.getDisplayStatusText(getContext(), status));
        mTextStatusDescription.setVisibility(GONE);
    }

    /**
     * To set getDisplayStatusText description.
     *
     * @param statusDescription
     */
    public void setStatusDescription(String statusDescription) {
        mTextStatusDescription.setText(statusDescription);
    }

    /**
     * To update UI when navigated from history
     */
    public void setStatusEnabled() {
        mTextStatus.setEnabled(true);
        mTextStatusDescription.setVisibility(VISIBLE);
    }
}
