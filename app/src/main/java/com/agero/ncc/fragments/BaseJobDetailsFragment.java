package com.agero.ncc.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.ChatActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.dispatcher.fragments.JobDetailsFragment;
import com.agero.ncc.model.CustomerInfoModel;
import com.agero.ncc.model.ExtenuationCircumstancesItem;
import com.agero.ncc.model.Facility;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Profile;
import com.agero.ncc.model.Status;
import com.agero.ncc.retrofit.NccApi;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.DownloadDirections;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.JobDetailCallBottomSheetDialog;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.vicmikhailau.maskededittext.MaskedFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.chatsdk.core.dao.Thread;
import co.chatsdk.core.dao.User;
import co.chatsdk.core.interfaces.ThreadType;
import co.chatsdk.core.session.NM;
import co.chatsdk.core.session.StorageManager;
import co.chatsdk.firebase.wrappers.UserWrapper;
import co.chatsdk.ui.manager.InterfaceManager;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.agero.ncc.adapter.DispatcherJobsAdapter.JOB_DISPLAY_TIME_LIMIT_SEC;
import static com.agero.ncc.utils.DateTimeUtils.getTimeDifference;
import static com.agero.ncc.utils.NccConstants.JOB_DISPATCH_SOURCE_ASM_VRM;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_ASSIGNED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_DECLINED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_DESTINATION_ARRIVAL;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_EN_ROUTE;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_EXPIRED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_JOB_COMPLETED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_OFFERED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_ON_SCENE;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_REJECTED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_TOWING;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_BLOCKED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_DELETED;
import static com.agero.ncc.utils.NccConstants.USER_STATUS_INACTIVE;

public class BaseJobDetailsFragment extends BaseStatusFragment
        implements OnMapReadyCallback, HomeActivity.JobDetailToolbarLisener,
        BaseFragment.OnSingleChoiceListener {
    private static final PatternItem DASH = new Dash(20);
    private static final PatternItem GAP = new Gap(10);
    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DASH);
    private static final int TOW_JOB_INITIAL_ETA = 45;
    private static final int OTHER_JOB_ETA = 30;
    protected String mDispatchNumber;
    protected boolean isItFromHistory;
    protected boolean isFromSearch;
    protected boolean isPermissionDenied;
    protected HomeActivity mHomeActivity;
    protected UserError mUserError;
    protected GoogleMap mMap;
    protected String selectedNumbertoCall;
    protected JobDetail mCurrentJob;
    protected Profile mUserProfile;
    protected DatabaseReference myRef;
    protected DatabaseReference mProfileReference, mFacilityReference;
    protected PolylineOptions mLineOptions, mFacilityLineOptions;
    @BindView(R.id.text_job_id)
    TextView mTextJobId;
    @BindView(R.id.image_disablement_info)
    ImageView imageDisablementInfo;
    @BindView(R.id.text_disablement_updated_info)
    TextView textDisablementUpdatedInfo;
    @BindView(R.id.image_tow_info)
    ImageView imageTowInfo;
    @BindView(R.id.job_details_parent)
    ConstraintLayout mParentLayout;
    @BindView(R.id.constraint_status_info)
    ConstraintLayout mConstraintStatusInfo;
    @BindView(R.id.image_status_info_constraint_notes)
    ImageView mImageStatusInfoConstraintNotes;
    @BindView(R.id.text_job_detail_info_status)
    TextView mTextJobDetailInfoStatus;
    @BindView(R.id.text_job_detail_info_description)
    TextView mTextJobDetailInfoDescription;
    @BindView(R.id.text_label_tow)
    TextView mTextLabelTow;
    @BindView(R.id.frame_map_container)
    FrameLayout mFrameMapContainer;
    @BindView(R.id.constraint_notes)
    ConstraintLayout mConstraintNotes;
    @BindView(R.id.button_start_job)
    Button mButtonStartJob;
    @BindView(R.id.text_service)
    TextView mTextService;
    @BindView(R.id.text_service_status)
    TextView mTextServiceStatus;
    @BindView(R.id.text_service_status_description)
    TextView mTextServiceStatusDescription;
    @BindView(R.id.view_border_status)
    View mViewBorderStatus;
    @BindView(R.id.text_service_eta)
    TextView mTextServiceEta;
    @BindView(R.id.text_service_quoted_eta_description)
    TextView mTextServiceQuotedEtaDescription;
    @BindView(R.id.view_border_eta)
    View mViewBorderEta;
    @BindView(R.id.text_service_driver)
    TextView mTextServiceDriver;
    @BindView(R.id.text_service_driver_description)
    TextView mTextServiceDriverDescription;
    @BindView(R.id.image_driver)
    ImageView mImageDriver;
    @BindView(R.id.view_border_driver)
    View mViewBorderDriver;
    @BindView(R.id.text_service_reason)
    TextView mTextServiceReason;
    @BindView(R.id.text_service_reason_description)
    TextView mTextServiceReasonDescription;
    @BindView(R.id.view_border_reason)
    View mViewBorderReason;
    @BindView(R.id.text_service_service)
    TextView mTextServiceService;
    @BindView(R.id.text_service_service_description)
    TextView mTextServiceServiceDescription;
    @BindView(R.id.view_border_service)
    View mViewBorderService;
    @BindView(R.id.text_service_equipment)
    TextView mTextServiceEquipment;
    @BindView(R.id.text_service_equipment_description)
    TextView mTextServiceEquipmentDescription;
    @BindView(R.id.view_border_equipment)
    View mViewBorderEquipment;
    @BindView(R.id.text_service_invoice)
    TextView mTextServiceInvoice;
    @BindView(R.id.text_service_invoice_description)
    TextView mTextServiceInvoiceDescription;
    @BindView(R.id.view_border_invoice)
    View mViewBorderInvoice;
    @BindView(R.id.text_service_miles)
    TextView mTextServiceMiles;
    @BindView(R.id.text_service_miles_description)
    TextView mTextServiceMilesDescription;
    @BindView(R.id.view_border_miles)
    View mViewBorderMiles;
    @BindView(R.id.text_disablement)
    TextView mTextDisablement;
    @BindView(R.id.text_disablement_address)
    TextView mTextDisablementAddress;
    @BindView(R.id.text_disablement_address_description)
    TextView mTextDisablementAddressDescription;
    @BindView(R.id.view_border_address)
    View mViewBorderAddress;
    @BindView(R.id.text_disablement_type)
    TextView mTextDisablementType;
    @BindView(R.id.text_disablement_type_description)
    TextView mTextDisablementTypeDescription;
    @BindView(R.id.view_border_type)
    View mViewBorderType;
    @BindView(R.id.text_disablement_garage)
    TextView mTextDisablementGarage;
    @BindView(R.id.text_disablement_garage_description)
    TextView mTextDisablementGarageDescription;
    @BindView(R.id.view_border_garage)
    View mViewBorderGarage;
    @BindView(R.id.text_tow_garage)
    TextView mTextTowGarage;
    @BindView(R.id.text_tow_garage_description)
    TextView mTextTowGarageDescription;
    @BindView(R.id.view_tow_border_garage)
    View mViewTowBorderGarage;
    @BindView(R.id.text_disablement_cross_street)
    TextView mTextDisablementCrossStreet;
    @BindView(R.id.text_disablement_cross_street_description)
    TextView mTextDisablementCrossStreetDescription;
    @BindView(R.id.view_border_cross_street)
    View mViewBorderCrossStreet;
    @BindView(R.id.text_disablement_with_vehicle)
    TextView mTextDisablementWithVehicle;
    @BindView(R.id.text_disablement_with_vehicle_description)
    TextView mTextDisablementWithVehicleDescription;
    @BindView(R.id.view_border_with_vehicle)
    View mViewBorderWithVehicle;
    @BindView(R.id.text_tow_destination)
    TextView mTextTowDestination;
    @BindView(R.id.text_tow_destination_address)
    TextView mTextTowDestinationAddress;
    @BindView(R.id.text_tow_destination_address_description)
    TextView mTextTowDestinationAddressDescription;
    @BindView(R.id.view_border_tow_destination_address)
    View mViewBorderTowDestinationAddress;
    @BindView(R.id.text_tow_destination_type)
    TextView mTextTowDestinationType;
    @BindView(R.id.text_tow_destination_type_description)
    TextView mTextTowDestinationTypeDescription;
    @BindView(R.id.view_border_tow_destination_type)
    View mViewBorderTowDestinationType;
    @BindView(R.id.text_tow_destination_cross_street)
    TextView mTextTowDestinationCrossStreet;
    @BindView(R.id.text_tow_destination_cross_street_description)
    TextView mTextTowDestinationCrossStreetDescription;
    @BindView(R.id.view_border_tow_destination_cross_street)
    View mViewBorderTowDestinationCrossStreet;
    @BindView(R.id.text_tow_destination_night_drop_off)
    TextView mTextTowDestinationNightDropOff;
    @BindView(R.id.text_tow_destination_night_drop_off_description)
    TextView mTextTowDestinationNightDropOffDescription;
    @BindView(R.id.view_border_tow_destination_night_drop_off)
    View mViewBorderTowDestinationNightDropOff;
    @BindView(R.id.text_tow_destination_get_vehicle_storage)
    TextView mTextTowDestinationGetVehicleStorage;
    @BindView(R.id.text_customer_vehicle)
    TextView mTextCustomerVehicle;
    @BindView(R.id.text_customer)
    TextView mTextCustomer;
    @BindView(R.id.text_customer_description)
    TextView mTextCustomerDescription;
    @BindView(R.id.customer_name_progress_bar_lay)
    RelativeLayout mCustomerNameProgressBarLayout;
    @BindView(R.id.view_border_customer)
    View mViewBorderCustomer;
    @BindView(R.id.text_vehicle_coverage)
    TextView mTextVehicleCoverage;
    @BindView(R.id.text_vehicle_coverage_description)
    TextView mTextVehicleCoverageDescription;
    @BindView(R.id.view_border_vehicle_coverage)
    View mViewBorderVehicleCoverage;

    @BindView(R.id.text_vehicle_carrier)
    TextView mTextVehicleCarrier;
    @BindView(R.id.text_vehicle_carrier_description)
    TextView mTextVehicleCarrierDescription;
    @BindView(R.id.view_border_carrier_coverage)
    View mViewBorderCarrierCoverage;
    @BindView(R.id.text_vehicle_claim_number)
    TextView mTextVehicleClaimNumber;
    @BindView(R.id.text_vehicle_claim_number_description)
    TextView mTextVehicleClaimNumDescription;
    @BindView(R.id.view_border_carrier_claim_number)
    View mViewBorderClaimNumber;

    @BindView(R.id.text_vehicle)
    TextView mTextVehicle;
    @BindView(R.id.text_vehicle_description)
    TextView mTextVehicleDescription;
    @BindView(R.id.view_border_vehicle)
    View mViewBorderVehicle;
    @BindView(R.id.text_vehicle_color)
    TextView mTextVehicleColor;
    @BindView(R.id.text_vehicle_color_description)
    TextView mTextVehicleColorDescription;
    @BindView(R.id.view_border_vehicle_color)
    View mViewBorderVehicleColor;
    @BindView(R.id.text_vehicle_plate)
    TextView mTextVehiclePlate;
    @BindView(R.id.text_vehicle_plate_description)
    TextView mTextVehiclePlateDescription;
    @BindView(R.id.view_border_vehicle_plate)
    View mViewBorderVehiclePlate;
    @BindView(R.id.text_vehicle_vin)
    TextView mTextVehicleVin;
    @BindView(R.id.text_vehicle_vin_description)
    TextView mTextVehicleVinDescription;
    @BindView(R.id.text_vehicle_vin_number)
    TextView mTextVehicleVinNumberHint;
    @BindView(R.id.view_border_vehicle_vin)
    View mViewBorderVehicleVin;
    @BindView(R.id.text_vehicle_class)
    TextView mTextVehicleClass;
    @BindView(R.id.text_vehicle_class_description)
    TextView mTextVehicleClassDescription;
    @BindView(R.id.view_border_vehicle_class)
    View mViewBorderVehicleClass;
    @BindView(R.id.text_vehicle_drive_train)
    TextView mTextVehicleDriveTrain;
    @BindView(R.id.text_vehicle_drive_train_description)
    TextView mTextVehicleDriveTrainDescription;
    @BindView(R.id.view_border_vehicle_drive_train)
    View mTiewBorderVehicleDriveTrain;
    @BindView(R.id.text_vehicle_fuel)
    TextView mTextVehicleFuel;
    @BindView(R.id.text_vehicle_fuel_description)
    TextView mTextVehicleFuelDescription;
    @BindView(R.id.view_border_vehicle_fuel)
    View mViewBorderVehicleFuel;
    @BindView(R.id.constraint_job_detail)
    ConstraintLayout mConstraintJobDetail;
    @BindView(R.id.scrollView_jobdetail)
    ScrollView mScrollViewJobdetail;
    @BindView(R.id.text_disablement_vehicle_report)
    TextView mTextDisablementVehicleReport;
    @BindView(R.id.text_disablement_add_vehicle_report)
    TextView mTextDisablementAddVehicleReport;
    @BindView(R.id.view_border_with_vehicle_report)
    View mViewBorderVehicleReport;
    @BindView(R.id.text_disablement_signature)
    TextView mTextDisablementSignature;
    @BindView(R.id.text_disablement_get_signature)
    TextView mTextDisablementGetSignature;
    @BindView(R.id.view_border_signature)
    View mViewBorderSignature;
    @BindView(R.id.text_tow_destination_signature)
    TextView mTextTowDestinationSignature;
    @BindView(R.id.text_tow_destination_get_signature)
    TextView mTextTowDestinationGetSignature;
    @BindView(R.id.view_border_tow_destination_signature)
    View mViewBorderTowDestinationSignature;
    @BindView(R.id.include_job_detail_eta)
    View mIncludeEtaDispatcher;
    @BindView(R.id.include_job_detail_tow)
    View mIncludeJobDetailTow;
    @BindView(R.id.frame_map_overlay)
    View frameMapOverlay;
    @BindView(R.id.guideline_service)
    Guideline guidelineService;
    @BindView(R.id.guideline_tow)
    Guideline guidelineTow;
    @BindView(R.id.guideline_disablement)
    Guideline guidelineDisablement;
    @BindView(R.id.text_eta)
    TextView textEta;
    @BindView(R.id.text_eta_description)
    TextView mTextEtaDescription;
    @BindView(R.id.text_set_eta)
    TextView mTextSetEta;
    @BindView(R.id.view_border_decrease_eta)
    View viewBorderDecreaseEta;
    @BindView(R.id.image_eta_decrease)
    ImageView mImageEtaDecrease;
    @BindView(R.id.view_border_increase_eta)
    View viewBorderIncreaseEta;
    @BindView(R.id.image_eta_increase)
    ImageView mImageEtaIncrease;
    @BindView(R.id.constraint_eta_time)
    ConstraintLayout constraintEtaTime;
    @BindView(R.id.button_request)
    Button mButtonRequest;
    @BindView(R.id.image_restrict)
    ImageView mImageRestrict;
    @BindView(R.id.constraint_restricted)
    ConstraintLayout mConstraintRestricted;
    @BindView(R.id.view_eta)
    View mViewEta;
    //    @BindView(R.id.image_service_info)
//    ImageView mImageServiceInfo;
//    @BindView(R.id.text_service_updated_info)
//    TextView mTextServiceUpdatedInfo;
    //    @BindView(R.id.text_job_offer)
//    TextView mTextJobOffer;
    @BindView(R.id.image_exclusive_offer)
    ImageView mImageExclusiveOffer;
    @BindView(R.id.eta_request_lay)
    ConstraintLayout mEtaRequestLayout;
    @BindView(R.id.text_note_description)
    TextView mTextNoteDescription;
    @BindView(R.id.text_disablement_get_poi)
    TextView mTextDisablementGetPOI;
    @BindView((R.id.text_tow_destination_get_contact_number))
    TextView mTextTowGetContactNumber;
    @BindView(R.id.text_service_invoice_rate_modifier)
    TextView mTextServiceInvoiceRateModifier;
    @BindView((R.id.loc_of_keys))
    TextView mTextLocationOfKey;
    @BindView((R.id.loc_of_keys_description))
    TextView mTextLocationOfKeyDesc;
    @BindView((R.id.view_border_loc_of_keys))
    View mTextLocationOfKeyBorder;
    @BindView(R.id.view_border_tow_destination_storage)
    View mViewBorderTowDestinationStorage;
    String eventAction = "";
    String eventCategory = "";

    @BindView(R.id.text_service_release_fee)
    TextView mTextReleaseFee;
    @BindView(R.id.text_service_release_fee_description)
    TextView mTextReleaseFeeDesc;
    @BindView(R.id.view_border_release_fee)
    View mViewBorderReleaseFee;

    @BindView(R.id.text_tow_destination_stock_lot)
    TextView mTextStockLot;
    @BindView(R.id.text_tow_destination_stock_lot_description)
    TextView mTextStockLotDescription;
    @BindView(R.id.view_border_tow_destination_stock_lot)
    View mViewBorderStockLot;
//    @BindView(R.id.job_detail_map_layout)
//    ConstraintLayout mConstraintJobDetailsMap;
//    @BindView(R.id.job_detail_service_layout)
//    ConstraintLayout mConstraintJobDetailsService;
//    @BindView(R.id.job_detail_disablement_layout)
//    ConstraintLayout mConstraintJobDetailsDisablement;
//    @BindView(R.id.job_detail_tow_layout)
//    ConstraintLayout mConstraintJobDetailsTow;
//    @BindView(R.id.job_detail_customer_layout)
//    ConstraintLayout mConstraintJobDetailsCustomer;

    @Inject
    NccApi nccApi;
    Facility mFacility;
    int mEtaValue;
    PermissionListener phoneCallPermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(PermissionGrantedResponse response) {
            try {
                Utils.makePhoneCall(getActivity(), selectedNumbertoCall);
            } catch (Exception e) {
                mUserError.message = "Cannot make call";
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

            }

        }

        @Override
        public void onPermissionDenied(PermissionDeniedResponse response) {
            if (response.isPermanentlyDenied()) {
                isPermissionDenied = true;
            }
        }

        @Override
        public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
                                                       PermissionToken token) {
            token.continuePermissionRequest();
        }
    };
    Status currentStatusForUpdate;
    ArrayList<Status> statusHistoryForUpdate;
    private Disposable mTimerDisposable;
    private String mCustomerName = "";
    private long oldRetryTime = 0;
    ValueEventListener profileValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.hasChildren() && isAdded()) {
                try {
                    mUserProfile = dataSnapshot.getValue(Profile.class);

                    if (mUserProfile != null) {
                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("json", new Gson().toJson(mUserProfile));
                        mHomeActivity.mintlogEventExtraData("Base Job Details User Profile", extraDatas);
                    }

//                    hideProgress();
                    if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchAssignedToName())) {
                        mTextServiceDriverDescription.setText(mCurrentJob.getDispatchAssignedToName());
                    } else if (mUserProfile != null && !TextUtils.isEmpty(mUserProfile.getFirstName())) {
                        StringBuilder driverNameBuilder =
                                new StringBuilder(mUserProfile.getFirstName());
                        driverNameBuilder.append(" ");
                        driverNameBuilder.append(mUserProfile.getLastName());
                        mTextServiceDriverDescription.setText(driverNameBuilder.toString());
                    }

                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }
//            hideProgress();
            try {
                mProfileReference.removeEventListener(profileValueEventListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
//            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Base Job Details user profile Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDriverDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private int serviceUnFinished = 0;
    private LatLng mCurrentLatLng;
    ValueEventListener mFacilityChangeEventListner = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (dataSnapshot.hasChildren() && isAdded()) {
                try {
                    mFacility = dataSnapshot.getValue(Facility.class);

                    if (mFacility != null) {
                        HashMap<String, String> extraDatas = new HashMap<>();
                        extraDatas.put("json", new Gson().toJson(mFacility));
                        mHomeActivity.mintlogEventExtraData("Base Job Details User Facility", extraDatas);
                    }

//                    hideProgress();
                    if (isAdded()) {
                        updateFacilityInMap();
                        updateInvoiceDetails();
                    }
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
//            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Base Job Details Facility Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getFacilityDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private BaseJobDetailsFragment.IAcceptDecline mAcceptDecline = new IAcceptDecline() {
        @Override
        public void enableAcceptButton(boolean state) {
            if (mButtonRequest != null && isAdded()) {
                mButtonRequest.setEnabled(state);
                mButtonRequest.setClickable(state);
            }
        }

        @Override
        public void enableDeclineButton(boolean state) {
            if (mImageRestrict != null && isAdded()) {
                mImageRestrict.setClickable(state);
                mImageRestrict.setEnabled(state);
            }
        }
    };

    private CompositeDisposable disposables = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        view = inflater.inflate(R.layout.fragment_job_detail_dispatcher_new, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mHomeActivity = (HomeActivity) getActivity();
        mUserError = new UserError();
        mHomeActivity.resetJobId();
        mHomeActivity.setJobChatBadge(0);
        if (getResources().getBoolean(R.bool.isTablet)) {
            mHomeActivity.showBottomBar();
            if (isItFromHistory) {
                if (isFromSearch) {
                    mHomeActivity.showToolbar(getString(R.string.title_job_id),
                            getString(R.string.title_firestore_history));
                    mHomeActivity.setOnJobDetailToolbarLisener(this);
                } else {
                    mHomeActivity.showToolbar(getString(R.string.title_job_id),
                            getString(R.string.title_history));
                    mHomeActivity.setOnJobDetailToolbarLisener(this);
                }
            } else {
                mHomeActivity.showToolbar(getString(R.string.title_job_id),
                        getString(R.string.title_jobdetail));
                mHomeActivity.setOnJobDetailToolbarLisener(this);
            }
        } else {
            mHomeActivity.hideBottomBar();
            mHomeActivity.showToolbar(getString(R.string.title_job_id),
                    getString(R.string.title_jobdetail));
            mHomeActivity.setOnJobDetailToolbarLisener(this);
        }

        setVisiblityLayout(View.GONE);
        mHomeActivity.jobCallVisible(false);
        mEtaValue = Integer.parseInt(mTextSetEta.getText().toString());
        mTextSetEta.setText(
                mEtaValue + " " + getResources().getString(R.string.alert_update_eta_min));
        mImageEtaDecrease.setEnabled(true);
        mImageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus);
        if (Utils.isNetworkAvailable()) {
            mEtaRequestLayout.setVisibility(View.VISIBLE);
        } else {
            mEtaRequestLayout.setVisibility(View.GONE);
        }
        mConstraintNotes.setBackgroundResource(R.drawable.border_corner_history);
        mConstraintStatusInfo.setBackgroundResource(R.drawable.border_corner_history);

        GradientDrawable drawableJobDetail = (GradientDrawable) mConstraintNotes.getBackground();
        drawableJobDetail.setColor(getResources().getColor(R.color.jobdetail_notes));

        GradientDrawable drawableConstraint = (GradientDrawable) mConstraintStatusInfo.getBackground();
        drawableConstraint.setColor(getResources().getColor(R.color.jobdetail_status_info_completed_background_green));


        view.findViewById(R.id.job_details_parent1).setVisibility(View.GONE);
        view.findViewById(R.id.p1).setVisibility(View.VISIBLE);

        return superView;
    }

    private View view;

    private void setVisiblityLayout(int visiblityLayout) {
        if (view != null && isAdded()) {
            view.findViewById(R.id.include_job_detail_map).setVisibility(visiblityLayout);
            view.findViewById(R.id.include_job_detail_service).setVisibility(visiblityLayout);
            view.findViewById(R.id.include_job_detail_disablement).setVisibility(visiblityLayout);
            view.findViewById(R.id.include_job_detail_customer).setVisibility(visiblityLayout);
            if (visiblityLayout == View.VISIBLE && Utils.isTow(mCurrentJob.getServiceTypeCode()) && mCurrentJob.getTowLocation() != null
                    && mCurrentJob.getTowLocation().getAddressLine1() != null
                    && mCurrentJob.getTowLocation().getCity() != null
                    && mCurrentJob.getTowLocation().getState() != null) {
                view.findViewById(R.id.include_job_detail_tow).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.include_job_detail_tow).setVisibility(View.GONE);
            }
        }
    }

    private void getFacilityDataFromFirebase() {

//        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    mFacilityReference = database.getReference("Facility/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")).getRef();
                    mFacilityReference.keepSynced(true);
                    mFacilityReference.addValueEventListener(mFacilityChangeEventListner);
                }
            }

            @Override
            public void onRefreshFailure() {
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void getDriverDataFromFirebase() {
//        showProgress();
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    mProfileReference = myRef.getRoot()
                            .child("Users")
                            .child(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))
                            .child(String.valueOf(mCurrentJob.getDispatchAssignedToId()));

                    mProfileReference.addValueEventListener(profileValueEventListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                if (isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    protected void showETAUpdated() {
        if (mPrefs.getBoolean(NccConstants.SHOW_UPDATE_ETA_BAR, false)) {
            mEditor.putBoolean(NccConstants.SHOW_UPDATE_ETA_BAR, false).commit();
            Snackbar.make(mParentLayout, getString(R.string.eta_updated), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCurrentJob != null && mHomeActivity != null && mCurrentJob.getDispatchId() != null) {


            if (getResources().getBoolean(R.bool.isTablet)) {
                mHomeActivity.showBottomBar();
                if (isItFromHistory) {
                    if (isFromSearch) {
                        mHomeActivity.showToolbar(getString(R.string.title_job_id) + (mCurrentJob.getDispatchId() == null ? ""
                                        : UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId())),
                                getString(R.string.title_firestore_history));
                        mHomeActivity.setOnJobDetailToolbarLisener(this);
                    } else {
                        mHomeActivity.showToolbar(getString(R.string.title_job_id) + (mCurrentJob.getDispatchId() == null ? ""
                                        : UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId())),
                                getString(R.string.title_history));
                        mHomeActivity.setOnJobDetailToolbarLisener(this);
                    }
                } else {
                    mHomeActivity.showToolbar(getString(R.string.title_job_id) + (mCurrentJob.getDispatchId() == null ? ""
                                    : UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId())),
                            getString(R.string.title_jobdetail));
                    mHomeActivity.setOnJobDetailToolbarLisener(this);
                }
            } else {
                mHomeActivity.showToolbar(getString(R.string.title_job_id) + (mCurrentJob.getDispatchId() == null ? ""
                                : UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId())),
                        getString(R.string.title_jobdetail));
                mHomeActivity.setOnJobDetailToolbarLisener(this);
            }


            /*mHomeActivity.showToolbar(
                    getString(R.string.title_job_id) + (mCurrentJob.getDispatchId() == null ? ""
                            : UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId())), getString(R.string.title_jobdetail));
            mHomeActivity.setOnJobDetailToolbarLisener(this);*/
        }
        if (!isItFromHistory) {
            showETAUpdated();
        }
    }

    @Override
    public void showProgress() {
        if (serviceUnFinished <= 0) {
            super.showProgress();
        }
        serviceUnFinished++;
    }

    @Override
    public void hideProgress() {
        serviceUnFinished--;
        if (serviceUnFinished <= 0) {
            super.hideProgress();
        }
    }

    protected void updateDriverJobDetails(DataSnapshot dataSnapshot) {
        mHomeActivity.setCurrentThreadName("");
        if (dataSnapshot.hasChildren()) {
            if (isAdded()) {
                try {
                    mCurrentJob = dataSnapshot.getValue(JobDetail.class);
                    loadJob();
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }
        } else {
            if (mHomeActivity != null && !mHomeActivity.isFinishing()) {
                Toast.makeText(mHomeActivity, "Job is moved to History", Toast.LENGTH_SHORT).show();
                if (!getResources().getBoolean(R.bool.isTablet)) {
                    mHomeActivity.popBackStackImmediate();
                }
            }
        }
    }

    public void loadJob() {
        try {
            Gson gson = new Gson();
            Timber.d(gson.toJson(mCurrentJob));
            if (NccConstants.IS_GEOFENCE_ENABLED) {
                SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mCurrentJob);
            }

            if (mCurrentJob != null && isMyJob() || !mHomeActivity.isLoggedInDriver()) {
                Crashlytics.setString("DispatchId", ""+mCurrentJob.getDispatchId());
                if (isAdded() && isVisible()) {

                    if (!getResources().getBoolean(R.bool.isTablet)) {
                        mHomeActivity.showToolbar(
                                getString(R.string.title_job_id) + (mCurrentJob.getDispatchId() == null ? ""
                                        : UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId())), getString(R.string.title_jobdetail));
                        mHomeActivity.setOnJobDetailToolbarLisener(this);
                    }
                }
                if (Utils.isTow(mCurrentJob.getServiceTypeCode()) && !isASMJob()) {
                    mEtaValue = TOW_JOB_INITIAL_ETA;
                    mTextSetEta.setText(mEtaValue + " " + getResources().getString(R.string.alert_update_eta_min));
                } else {
                    mEtaValue = OTHER_JOB_ETA;
                    mTextSetEta.setText(mEtaValue + " " + getResources().getString(R.string.alert_update_eta_min));
                }
                if (Utils.isNetworkAvailable()) {
                    getFacilityDataFromFirebase();
                    updateDriverUiFromApi();
                    updateMapWithMarkers();
                    updateExtenuationCircumstances();
                } else {
                    mUserError.title = "";
                    mUserError.message = getString(R.string.network_error_message);
                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                }

                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", gson.toJson(mCurrentJob));
                mHomeActivity.mintlogEventExtraData(" Job Details", extraDatas);
            } else {
                //job assigned to some other driver.
                if (!getResources().getBoolean(R.bool.isTablet)) {
                    mHomeActivity.popBackStackImmediate();
                }
            }
        } catch (Exception e) {
            mHomeActivity.mintLogException(e);
        }
    }

    private boolean isASMJob() {
        return !TextUtils.isEmpty(mCurrentJob.getServiceTypeCode()) && !TextUtils.isEmpty(mCurrentJob.getDispatchSource()) && JOB_DISPATCH_SOURCE_ASM_VRM.equalsIgnoreCase(mCurrentJob.getDispatchSource()) && mCurrentJob.getServiceTypeCode().toUpperCase().startsWith(NccConstants.JOB_SERVICE_TYPE_ASM);
    }

    private void updateExtenuationCircumstances() {
        List<ExtenuationCircumstancesItem> mExtenuationCircumstancesItems = mCurrentJob.getExtenuationCircumstances();
        StringBuilder mExtenuationCircumstances = new StringBuilder();
        if (mExtenuationCircumstancesItems != null) {
            for (int i = 0; i < mExtenuationCircumstancesItems.size(); i++) {
                mExtenuationCircumstances.append(mExtenuationCircumstancesItems.get(i).getExtenuationDetailText());
                if (i != mExtenuationCircumstancesItems.size() - 1) {
                    mExtenuationCircumstances.append(", ");
                }
            }
        }
        mConstraintNotes.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(mCurrentJob.getDispatchSource()) && JOB_DISPATCH_SOURCE_ASM_VRM.equalsIgnoreCase(mCurrentJob.getDispatchSource())) {
            String impNotes = "";
            if (!TextUtils.isEmpty(mCurrentJob.getDamageInformation())) {
                impNotes = mCurrentJob.getDamageInformation();
            }
            if (!TextUtils.isEmpty(mExtenuationCircumstances)) {
                impNotes += ", " + mExtenuationCircumstances;
            }
            if (impNotes.isEmpty()) {
                mConstraintNotes.setVisibility(View.GONE);
            } else {
                mTextNoteDescription.setText(impNotes);
            }
        } else if (!TextUtils.isEmpty(mExtenuationCircumstances)) {
            mTextNoteDescription.setText(mExtenuationCircumstances);
        } else {
            mConstraintNotes.setVisibility(View.GONE);
        }
    }

    private void updateInvoiceDetails() {
        if (mFacility != null && mCurrentJob != null) {
            if (mHomeActivity.isLoggedInDriver()) {
                if (mFacility.getShowCostToDriver()) {
                    mTextServiceInvoice.setVisibility(View.VISIBLE);
                    mTextServiceInvoiceDescription.setVisibility(View.VISIBLE);
//                    mViewBorderInvoice.setVisibility(View.VISIBLE);

                    showRateModifier();
                } else {
                    mTextServiceInvoice.setVisibility(View.GONE);
                    mTextServiceInvoiceDescription.setVisibility(View.GONE);
//                    mViewBorderInvoice.setVisibility(View.GONE);
                }
            } else {
                if (mFacility.getShowCostToDispatcher()) {
                    mTextServiceInvoice.setVisibility(View.VISIBLE);
                    mTextServiceInvoiceDescription.setVisibility(View.VISIBLE);
                    mViewBorderMiles.setVisibility(View.VISIBLE);
//                    mViewBorderInvoice.setVisibility(View.VISIBLE);
                    showRateModifier();
                } else {
                    mTextServiceInvoice.setVisibility(View.GONE);
                    mTextServiceInvoiceDescription.setVisibility(View.GONE);
                    mViewBorderMiles.setVisibility(View.GONE);
//                    mViewBorderInvoice.setVisibility(View.GONE);
                }
            }
        }
    }

    private void showRateModifier() {
        float amount = 0f;
        if (mCurrentJob.getCostBreakDown() != null && mCurrentJob.getCostBreakDown().getNightCost() != null && mCurrentJob.getCostBreakDown().getNightCost() > 0F) {
            amount += mCurrentJob.getCostBreakDown().getNightCost();
//            mTextServiceInvoiceRateModifier.setVisibility(View.VISIBLE);
//            mTextServiceInvoiceRateModifier.setText(String.format("$%s", ) + " rate modifier included");
        }
        if (mCurrentJob.getCostBreakDown() != null && mCurrentJob.getCostBreakDown().getSundayCost() != null && mCurrentJob.getCostBreakDown().getSundayCost() > 0F) {

            amount += mCurrentJob.getCostBreakDown().getSundayCost();
        }

        if (amount > 0f) {
            mTextServiceInvoiceRateModifier.setVisibility(View.VISIBLE);
            mTextServiceInvoiceRateModifier.setText(String.format("$%s", amount) + " rate modifier included");
        } else {
            mTextServiceInvoiceRateModifier.setVisibility(View.GONE);
        }
    }

    protected void updateDriverUiFromApi() {

        getDriverDataFromFirebase();
//        if(mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDriverAssignedByName())){
//            mTextServiceDriverDescription.setText(mCurrentJob.getDriverAssignedByName());
//        }else{
//            mTextServiceDriverDescription.setText(getString(R.string.not_available));
//        }

        enableClicks();

        if (mHomeActivity.isLoggedInDriver()) {
            //            Hide UI elements that are not related to Driver
            setJobId();
            mTextServiceDriver.setVisibility(View.GONE);
            mTextServiceDriverDescription.setVisibility(View.GONE);
            mViewBorderDriver.setVisibility(View.GONE);
            mTextServiceInvoice.setVisibility(View.GONE);
            mTextServiceInvoiceDescription.setVisibility(View.GONE);
            mTextServiceInvoiceRateModifier.setVisibility(View.GONE);
//            mViewBorderInvoice.setVisibility(View.GONE);
            mIncludeEtaDispatcher.setVisibility(View.GONE);
            mViewEta.setVisibility(View.GONE);
//            if (mCurrentJob != null && (mCurrentJob.getEtaExtensionCount() == null || mCurrentJob.getEtaExtensionCount() == 0)) {
//                mTextServiceQuotedEtaDescription.setTextColor(
//                        getResources().getColor(R.color.ncc_blue));
//            } else {
//                mTextServiceQuotedEtaDescription.setTextColor(
//                        getResources().getColor(R.color.ncc_black));
//            }
//            mTextDisablementAddVehicleReport.setVisibility(View.VISIBLE);
//            mTextDisablementVehicleReport.setVisibility(View.VISIBLE);
//            mTextDisablementSignature.setVisibility(View.VISIBLE);
//            mTextDisablementGetSignature.setVisibility(View.VISIBLE);
//            mViewBorderVehicleReport.setVisibility(View.VISIBLE);
            mViewBorderWithVehicle.setVisibility(View.VISIBLE);
//            mTextTowDestinationSignature.setVisibility(View.VISIBLE);
//            mTextTowDestinationGetSignature.setVisibility(View.VISIBLE);
            mViewBorderTowDestinationNightDropOff.setVisibility(View.VISIBLE);
//            mTextVehicleVinDescription.setEnabled(true);
            mTextServiceDriverDescription.setVisibility(View.GONE);
            mTextServiceDriver.setVisibility(View.GONE);
            mViewBorderDriver.setVisibility(View.GONE);
            if (isItFromHistory) {
                mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_black));
//                mHomeActivity.jobOptionsVisible(false);
                mHomeActivity.jobCallVisible(false);
            } else {
                mHomeActivity.jobCallVisible(true);
                mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
            }

            if (mCurrentJob.getReportAttachment() == null) {
                mTextDisablementAddVehicleReport.setText(
                        getString(R.string.job_details_add_photos));
            } else if (mCurrentJob.getReportAttachment() != null && mCurrentJob.getReportAttachment().getDocuments() == null && TextUtils.isEmpty(mCurrentJob.getReportAttachment().getNotes())) {
                mTextDisablementAddVehicleReport.setText(
                        getString(R.string.job_details_add_photos));
            } else {
                mTextDisablementAddVehicleReport.setText(
                        getString(R.string.job_details_view_photos));
            }
        } else {
            setJobId();
            mButtonStartJob.setVisibility(View.GONE);
            mTextServiceInvoice.setVisibility(View.GONE);
            mTextServiceInvoiceDescription.setVisibility(View.GONE);
            mTextServiceInvoiceRateModifier.setVisibility(View.GONE);
//            mViewBorderInvoice.setVisibility(View.GONE);
//            if (mCurrentJob != null && (mCurrentJob.getEtaExtensionCount() == null || mCurrentJob.getEtaExtensionCount() == 0)) {
//                mTextServiceQuotedEtaDescription.setTextColor(
//                        getResources().getColor(R.color.ncc_blue));
//            } else {
//                mTextServiceQuotedEtaDescription.setTextColor(
//                        getResources().getColor(R.color.ncc_black));
//                mTextServiceQuotedEtaDescription.setText(getResources().getString(R.string.not_available));
//            }
            //            mTextDisablementAddVehicleReport.setVisibility(View.GONE);
            //            mTextDisablementVehicleReport.setVisibility(View.GONE);
            //            mTextDisablementSignature.setVisibility(View.GONE);
            //            mTextDisablementGetSignature.setVisibility(View.GONE);
            //            mViewBorderWithVehicle.setVisibility(View.GONE);
            //            mViewBorderVehicleReport.setVisibility(View.GONE);
            //            mTextTowDestinationSignature.setVisibility(View.GONE);
            //            mTextTowDestinationGetSignature.setVisibility(View.GONE);
            //            mViewBorderTowDestinationNightDropOff.setVisibility(View.GONE);
            mTextVehicleVinDescription.setEnabled(false);
            mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        }
        if (mCurrentJob.getVehicle() != null && mCurrentJob.getServiceTypeCode() != null && mCurrentJob.getVehicle().getYear() != null
                && mCurrentJob.getVehicle().getMake() != null && mCurrentJob.getVehicle().getModel() != null
                && mCurrentJob.getVehicle().getColor() != null) {
            mTextLabelTow.setText(Utils.getDisplayServiceTypes(mHomeActivity, mCurrentJob.getServiceTypeCode())
                    + " - "
                    + mCurrentJob.getVehicle().getYear()
                    + " "
                    + mCurrentJob.getVehicle().getMake()
                    + " "
                    + mCurrentJob.getVehicle().getModel()
                    + " "
                    + mCurrentJob.getVehicle().getColor());
        }

        //Service
        if (mCurrentJob != null && mCurrentJob.getCurrentStatus() != null) {
            mTextServiceStatusDescription.setText(Utils.getDisplayStatusText(mHomeActivity, mCurrentJob.getCurrentStatus().getStatusCode() != null ? mCurrentJob.getCurrentStatus().getStatusCode() : ""));
        }
//        if (mCurrentJob != null && mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getStatusCode() != null && (mCurrentJob.getEtaExtensionCount() == null || mCurrentJob.getEtaExtensionCount() == 0) && (NccConstants.JOB_STATUS_ASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_EN_ROUTE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()))) {
//            mTextServiceQuotedEtaDescription.setTextColor(
//                    getResources().getColor(R.color.ncc_blue));
//        } else {
//            mTextServiceQuotedEtaDescription.setTextColor(
//                    getResources().getColor(R.color.ncc_black));
//        }


        if (mCurrentJob.getEtaStatus() != null && mCurrentJob.getEtaStatus().equalsIgnoreCase("REQUEST") && !isItFromHistory) {
            if (mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(JOB_STATUS_ASSIGNED) || mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(JOB_STATUS_EN_ROUTE)) {
                mEtaRequestLayout.setVisibility(View.VISIBLE);
            } else {
                mEtaRequestLayout.setVisibility(View.GONE);
            }
        } else {
            mEtaRequestLayout.setVisibility(View.GONE);
        }

      /*  if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getStatusCode() != null && NccConstants.JOB_STATUS_ACCEPTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_CANCELLED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mTextServiceDriverDescription.setText(getString(R.string.not_available));
        } else {
            mTextServiceDriverDescription.setText(Utils.toCamelCase(mCurrentJob.getDispatchAssignedToName()));
        }*/

        if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchAssignedToName())) {
            mTextServiceDriverDescription.setText(Utils.toCamelCase(mCurrentJob.getDispatchAssignedToName()));
        } else {
            if (NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mTextServiceDriverDescription.setText(getString(R.string.title_assign_driver));
            } else {
                mTextServiceDriverDescription.setText(getString(R.string.not_available));
            }
        }

        mTextServiceReasonDescription.setText(TextUtils.isEmpty(mCurrentJob.getCallReason()) ? getString(R.string.not_available)
                : mCurrentJob.getCallReason());
        mTextServiceServiceDescription.setText(TextUtils.isEmpty(mCurrentJob.getServiceTypeCode()) ? getString(R.string.not_available)
                : Utils.getDisplayServiceTypes(mHomeActivity, mCurrentJob.getServiceTypeCode()));

        mTextLocationOfKey.setVisibility(View.GONE);
        mTextLocationOfKeyDesc.setVisibility(View.GONE);
        mTextLocationOfKeyBorder.setVisibility(View.GONE);

        if (mHomeActivity.isUserDispatcher() && !TextUtils.isEmpty(mCurrentJob.getServiceTypeCode()) && !TextUtils.isEmpty(mCurrentJob.getCallReasonSummary())
                && (mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_WINCH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_HWNH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_MWNH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_LWNH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_LKT) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_LKTSMITH))) {
            mTextLocationOfKey.setVisibility(View.VISIBLE);
            mTextLocationOfKeyDesc.setVisibility(View.VISIBLE);
            mTextLocationOfKeyBorder.setVisibility(View.VISIBLE);

            if (mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_WINCH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_HWNH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_MWNH) || mCurrentJob.getServiceTypeCode().equalsIgnoreCase(NccConstants.SERVICE_TYPE_LWNH)) {
                mTextLocationOfKey.setText(getString(R.string.distance_from_road));
            } else {
                mTextLocationOfKey.setText(getString(R.string.location_of_keys));
            }
            mTextLocationOfKeyDesc.setText(mCurrentJob.getCallReasonSummary());
        }

        if (mCurrentJob.getEquipmentList() != null) {
//            mTextServiceEquipmentDescription.setText(mCurrentJob.getEquipmentList() != null ? mCurrentJob.getEquipmentList().get(0).getEquipmentTypeName() : "");
//        } else {
//            mTextServiceEquipmentDescription.setText(getString(R.string.not_available));

            mTextServiceEquipmentDescription.setText(
                    TextUtils.isEmpty(mCurrentJob.getEquipmentList().get(0).getEquipmentTypeName()) ? getString(R.string.not_available)
                            : mCurrentJob.getEquipmentList().get(0).getEquipmentTypeName());
        } else {
            mTextServiceEquipmentDescription.setText(getString(R.string.not_available));
        }
        mTextServiceInvoiceDescription.setText(
                String.format("$%s", mCurrentJob.getInvoiceAmount()));

        if (Utils.isTow(mCurrentJob.getServiceTypeCode())) {

            String milesText = "";

            if (mCurrentJob.getMilesToDisablementLoc() != 0F) {
                milesText = "En-Route " + mCurrentJob.getMilesToDisablementLoc();
            }


            if (mCurrentJob.getMilesToTowLoc() != 0F) {
                milesText += " " + getString(R.string.dot) + "  Towed " + mCurrentJob.getMilesToTowLoc();
            }


            if (mCurrentJob.getDeadHeadMiles() != 0F) {
                milesText += " " + getString(R.string.dot) + " DeadHead " + mCurrentJob.getDeadHeadMiles();
            }
            if (TextUtils.isEmpty(milesText)) {
                mTextServiceMilesDescription.setText(getString(R.string.not_available));
            } else {
                mTextServiceMilesDescription.setText(milesText);
            }

        } else {
            String milesText = "";

            if (mCurrentJob.getMilesToDisablementLoc() != 0F) {
                milesText = "En-Route " + mCurrentJob.getMilesToDisablementLoc();
            }

            if (mCurrentJob.getDeadHeadMiles() != 0F) {
                milesText += " " + getString(R.string.dot) + " DeadHead " + mCurrentJob.getDeadHeadMiles();
            }
            if (TextUtils.isEmpty(milesText)) {
                mTextServiceMilesDescription.setText(getString(R.string.not_available));
            } else {
                mTextServiceMilesDescription.setText(milesText);
            }
        }

        //Disablement
        String disablementLocationBuilder = mCurrentJob.getDisablementLocation().getAddressLine1()
                + ", ";
        if (!TextUtils.isEmpty(mCurrentJob.getDisablementLocation().getAddressLine2())) {
            disablementLocationBuilder += mCurrentJob.getDisablementLocation().getAddressLine2() + ", ";
        }
        disablementLocationBuilder += mCurrentJob.getDisablementLocation().getCity()
                + ", ";
        mTextDisablementAddressDescription.setText(Utils.toCamelCase(disablementLocationBuilder) + mCurrentJob.getDisablementLocation().getState() + ", " + mCurrentJob.getDisablementLocation().getPostalCode());

        mTextDisablementTypeDescription.setText(TextUtils.isEmpty(mCurrentJob.getDisablementLocation().getLocationType()) ? getString(R.string.not_available)
                : mCurrentJob.getDisablementLocation().getLocationType());


        if (mCurrentJob.getDisablementLocation() != null && !TextUtils.isEmpty(mCurrentJob.getDisablementLocation().getLocationType()) && mCurrentJob.getDisablementLocation().getLocationType().contains("Garage")) {
            String garageInfo = "";

            if (mCurrentJob.getDisablementLocation().getGarageClearanceFeet() != 0) {
                garageInfo = mCurrentJob.getDisablementLocation().getGarageClearanceFeet() + " ft ";
            }

            if (mCurrentJob.getDisablementLocation().getGarageClearanceInches() != 0) {
                if (!TextUtils.isEmpty(garageInfo)) {
                    garageInfo += ".";
                }
                garageInfo += mCurrentJob.getDisablementLocation().getGarageClearanceInches() + " in ";
            }

            if (mCurrentJob.getDisablementLocation().getGarageLevel() != 0) {
                if (!TextUtils.isEmpty(garageInfo)) {
                    garageInfo += getString(R.string.dot);
                }
                garageInfo += " Level " + mCurrentJob.getDisablementLocation().getGarageLevel();
            }

            Object obj = mCurrentJob.getDisablementLocation().getGarageSection();

            String garageSection = "";
            if (obj != null && obj instanceof Integer) {
                garageSection = "" + (Integer) obj;
            } else if (obj != null && obj instanceof String) {
                garageSection = (String) obj;
            }

            if (!TextUtils.isEmpty(garageSection)) {
                if (!TextUtils.isEmpty(garageInfo)) {
                    garageInfo += getString(R.string.dot);
                }
                garageInfo += " Section " + garageSection;
            }

            mTextDisablementGarageDescription.setText(
                    garageInfo.isEmpty() ? getString(R.string.not_available)
                            : garageInfo);
        } else {
            mTextDisablementGarageDescription.setText(getString(R.string.not_available));
        }

        if (Utils.isTow(mCurrentJob.getServiceTypeCode()) && mCurrentJob.getTowLocation() != null && !TextUtils.isEmpty(mCurrentJob.getTowLocation().getLocationType()) && mCurrentJob.getTowLocation().getLocationType().contains("Garage")) {
            String garageInfoTow = "";

            if (mCurrentJob.getTowLocation().getGarageClearanceFeet() != 0) {
                garageInfoTow = mCurrentJob.getTowLocation().getGarageClearanceFeet() + " ft ";
            }

            if (mCurrentJob.getTowLocation().getGarageClearanceInches() != 0) {
                if (!garageInfoTow.isEmpty()) {
                    garageInfoTow += ".";
                }
                garageInfoTow += mCurrentJob.getTowLocation().getGarageClearanceInches() + " in ";
            }

            if (mCurrentJob.getTowLocation().getGarageLevel() != 0) {
                if (!garageInfoTow.isEmpty()) {
                    garageInfoTow += getString(R.string.dot);
                }
                garageInfoTow += " Level " + mCurrentJob.getTowLocation().getGarageLevel();
            }

            Object obj = mCurrentJob.getTowLocation().getGarageSection();

            String garageSection = "";
            if (obj != null && obj instanceof Integer) {
                garageSection = "" + (Integer) obj;
            } else if (obj != null && obj instanceof String) {
                garageSection = (String) obj;
            }

            if (!TextUtils.isEmpty(garageSection)) {
                if (!TextUtils.isEmpty(garageInfoTow)) {
                    garageInfoTow += getString(R.string.dot);
                }
                garageInfoTow += " Section " + garageSection;
            }

            mTextTowGarageDescription.setText(
                    garageInfoTow.isEmpty() ? getString(R.string.not_available)
                            : garageInfoTow);
        } else {
            mTextTowGarageDescription.setText(getString(R.string.not_available));
        }

        mTextDisablementCrossStreetDescription.setText(
                TextUtils.isEmpty(mCurrentJob.getDisablementLocation().getCrossStreet()) ? getString(R.string.not_available)
                        : mCurrentJob.getDisablementLocation().getCrossStreet());
        mTextDisablementWithVehicleDescription.setText(
                mCurrentJob.getIsCustomerWithVehicle() ? "Yes" : "No");
        mTextDisablementGetPOI.setText(TextUtils.isEmpty(mCurrentJob.getDisablementLocation().getPoi()) ? getString(R.string.not_available)
                : mCurrentJob.getDisablementLocation().getPoi());

        //Customer Vehicle
        mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_black));

        if (mCurrentJob.getCustomerName() != null && !TextUtils.isEmpty(mCurrentJob.getCustomerName())) {
            mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
            mTextCustomerDescription.setText(Utils.toCamelCase(mCurrentJob.getCustomerName()));
        } else {
            mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_black));
            mTextCustomerDescription.setText(getString(R.string.not_available));
        }
        if (mCurrentJob != null && (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(JOB_STATUS_JOB_COMPLETED))) {
            // If active and completed job show plate, customer and VIN

//            mTextCustomer.setVisibility(View.VISIBLE);
//            mTextCustomerDescription.setVisibility(View.VISIBLE);
//            mViewBorderCustomer.setVisibility(View.VISIBLE);

            mTextVehiclePlate.setVisibility(View.VISIBLE);
            mTextVehiclePlateDescription.setVisibility(View.VISIBLE);
            mViewBorderVehiclePlate.setVisibility(View.VISIBLE);

            mTextVehicleVin.setVisibility(View.VISIBLE);
            mTextVehicleVinDescription.setVisibility(View.VISIBLE);
            mViewBorderVehicleVin.setVisibility(View.VISIBLE);

            mTextDisablementAddVehicleReport.setVisibility(View.VISIBLE);
            mTextDisablementVehicleReport.setVisibility(View.VISIBLE);
            mViewBorderVehicleReport.setVisibility(View.VISIBLE);

            mTextDisablementSignature.setVisibility(View.VISIBLE);
            mTextDisablementGetSignature.setVisibility(View.VISIBLE);
            mViewBorderSignature.setVisibility(View.VISIBLE);

            mTextTowDestinationSignature.setVisibility(View.VISIBLE);
            mTextTowDestinationGetSignature.setVisibility(View.VISIBLE);
            mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);
           /* if(mHomeActivity.isLoggedInDriver()){
                mViewBorderMiles.setVisibility(View.GONE);
            }*/

        } else {
//            mTextCustomer.setVisibility(View.GONE);
//            mTextCustomerDescription.setVisibility(View.GONE);
//            mViewBorderCustomer.setVisibility(View.GONE);

            mTextVehiclePlate.setVisibility(View.GONE);
            mTextVehiclePlateDescription.setVisibility(View.GONE);
            mViewBorderVehiclePlate.setVisibility(View.GONE);

            mTextVehicleVin.setVisibility(View.GONE);
            mTextVehicleVinDescription.setVisibility(View.GONE);
            mViewBorderVehicleVin.setVisibility(View.GONE);

            mTextDisablementAddVehicleReport.setVisibility(View.GONE);
            mTextDisablementVehicleReport.setVisibility(View.GONE);
            mViewBorderVehicleReport.setVisibility(View.GONE);

            mTextDisablementSignature.setVisibility(View.GONE);
            mTextDisablementGetSignature.setVisibility(View.GONE);
//            mViewBorderSignature.setVisibility(View.GONE);

            mTextTowDestinationSignature.setVisibility(View.GONE);
            mTextTowDestinationGetSignature.setVisibility(View.GONE);
//            mViewBorderTowDestinationSignature.setVisibility(View.GONE);
        }

//        mTextVehicleCoverageDescription.setText(mCurrentJob.getCoverage());
        mTextVehicleCoverageDescription.setText(
                mCurrentJob.getCoverage() != null && !TextUtils.isEmpty(mCurrentJob.getCoverage()) ? mCurrentJob.getCoverage()
                        : getString(R.string.not_available));


        String vehicleDesc = "" + mCurrentJob.getVehicle().getYear();
        if (!TextUtils.isEmpty(mCurrentJob.getVehicle().getMake())) {
            vehicleDesc += " " + mCurrentJob.getVehicle().getMake();
        }
        if (!TextUtils.isEmpty(mCurrentJob.getVehicle().getModel())) {
            vehicleDesc += " " + mCurrentJob.getVehicle().getModel();
        }
        mTextVehicleDescription.setText(vehicleDesc);

        if (mCurrentJob.getVehicle() != null) {

            mTextVehicleColorDescription.setText(
                    mCurrentJob.getVehicle().getColor() != null && !TextUtils.isEmpty(mCurrentJob.getVehicle().getColor()) ? mCurrentJob.getVehicle().getColor()
                            : getString(R.string.not_available));

            mTextVehiclePlateDescription.setText(
                    mCurrentJob.getVehicle().getPlate() != null && !TextUtils.isEmpty(mCurrentJob.getVehicle().getPlate()) ? mCurrentJob.getVehicle().getPlate()
                            : getString(R.string.not_available));

//            if (mCurrentJob.getVinCaptureRequired())) {
//                mTextVehicleVinDescription.setEnabled(true);
//                mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
//                if (mCurrentJob.getCapturedVinInfo() == null || TextUtils.isEmpty(mCurrentJob.getCapturedVinInfo().getVin())) {
//                    mTextVehicleVinDescription.setText(getString(R.string.job_detail_text_scan_vin));
//                }else{
//                    mTextVehicleVinDescription.setText(mCurrentJob.getCapturedVinInfo().getVin());
//                }
//            }else if()


//
//            if (mCurrentJob.getVinCaptureRequired() && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
//                mTextVehicleVinDescription.setEnabled(true);
//                if (mCurrentJob.getVehicle().getVin() != null && TextUtils.isEmpty(mCurrentJob.getVehicle().getVin())) {
//                    mTextVehicleVinDescription.setText(getString(R.string.title_scanvin));
//                }
//            } else {
//                mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_black));
//                mTextVehicleVinDescription.setEnabled(false);
//                if (mCurrentJob.getVehicle().getVin() != null && TextUtils.isEmpty(mCurrentJob.getVehicle().getVin())) {
//                    mTextVehicleVinDescription.setText(getString(R.string.not_available));
//                }
//            }

            mTextVehicleClassDescription.setText(
                    mCurrentJob.getVehicle().getVehicleType() != null && !TextUtils.isEmpty(mCurrentJob.getVehicle().getVehicleType()) ? mCurrentJob.getVehicle().getVehicleType()
                            : getString(R.string.not_available));

            mTextVehicleDriveTrainDescription.setText(
                    mCurrentJob.getVehicle().getDriveTrainType() != null && !TextUtils.isEmpty(mCurrentJob.getVehicle().getDriveTrainType()) ? mCurrentJob.getVehicle().getDriveTrainType()
                            : getString(R.string.not_available));

            mTextVehicleFuelDescription.setText(
                    mCurrentJob.getVehicle().getFuelType() != null && !TextUtils.isEmpty(mCurrentJob.getVehicle().getFuelType()) ? mCurrentJob.getVehicle().getFuelType()
                            : getString(R.string.not_available));

//            mTextVehicleClassDescription.setText(mCurrentJob.getVehicle().getVehicleType());
//            mTextVehicleDriveTrainDescription.setText(mCurrentJob.getVehicle().getDriveTrainType());
//            mTextVehicleFuelDescription.setText(mCurrentJob.getVehicle().getFuelType());

        }


        if (mCurrentJob.getDisablementLocation().getSignature() != null) {
            mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
        } else {
            mTextDisablementGetSignature.setText(getString(R.string.job_details_get_signature));
        }
        if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
            mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
        } else {
            mTextTowDestinationGetSignature.setText(getString(R.string.job_details_get_signature));
        }

        //        To show Tow job related fields by checking the job type
        if (Utils.isTow(mCurrentJob.getServiceTypeCode()) && mCurrentJob.getTowLocation() != null
                && mCurrentJob.getTowLocation().getAddressLine1() != null
                && mCurrentJob.getTowLocation().getCity() != null
                && mCurrentJob.getTowLocation().getState() != null) {
            //Tow Destination
            String towLocationBuilder = mCurrentJob.getTowLocation().getAddressLine1()
                    + ", ";
            if (!TextUtils.isEmpty(mCurrentJob.getTowLocation().getAddressLine2())) {
                towLocationBuilder += mCurrentJob.getTowLocation().getAddressLine2() + ", ";
            }
            towLocationBuilder += mCurrentJob.getTowLocation().getCity()
                    + ", ";

            mTextTowDestinationAddressDescription.setText(Utils.toCamelCase(towLocationBuilder) + mCurrentJob.getTowLocation().getState() + ", " + mCurrentJob.getTowLocation().getPostalCode());
            mTextTowDestinationTypeDescription.setText(getString(R.string.not_available)); //mCurrentJob.getTowLocation().getType() ????
            mTextTowDestinationTypeDescription.setText(
                    TextUtils.isEmpty(mCurrentJob.getTowLocation().getLocationType().toString()) ? getString(R.string.not_available)
                            : mCurrentJob.getTowLocation().getLocationType());
            mTextTowDestinationCrossStreetDescription.setText(
                    TextUtils.isEmpty(mCurrentJob.getTowLocation().getCrossStreet().toString()) ? getString(R.string.not_available)
                            : mCurrentJob.getTowLocation().getCrossStreet().toString());
            mTextTowDestinationCrossStreetDescription.setTextColor(getResources().getColor(R.color.ncc_black));


            String phoneNumber = mCurrentJob.getTowLocation().getPhoneNumber().toString();
            MaskedFormatter formatter = new MaskedFormatter("(###) ###-####");
            if (TextUtils.isEmpty(phoneNumber)) {
                mTextTowGetContactNumber.setText(getString(R.string.not_available));
                mTextTowGetContactNumber.setTextColor(getResources().getColor(R.color.ncc_black));
                mTextTowGetContactNumber.setEnabled(false);
            } else if (isItFromHistory && NccConstants.JOB_STATUS_CANCELLED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                    || NccConstants.JOB_STATUS_GOA.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                    || NccConstants.JOB_STATUS_DENIED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mTextTowGetContactNumber.setText(formatter.formatString(phoneNumber));
                mTextTowGetContactNumber.setTextColor(getResources().getColor(R.color.ncc_black));
                mTextTowGetContactNumber.setEnabled(false);
            } else {
                mTextTowGetContactNumber.setText(formatter.formatString(phoneNumber));
                mTextTowGetContactNumber.setTextColor(getResources().getColor(R.color.ncc_blue));
                mTextTowGetContactNumber.setEnabled(true);
            }

            if (mCurrentJob.getTowLocation().getNightDropOff()) {
                mTextTowDestinationNightDropOffDescription.setText("Yes");
            } else {
                mTextTowDestinationNightDropOffDescription.setText("No");
            }

            if (mCurrentJob.getTowLocation().getIsTowToStorage()) {
                mTextTowDestinationGetVehicleStorage.setText("Yes");
            } else {
                mTextTowDestinationGetVehicleStorage.setText("No");
            }

        } else {
            mIncludeJobDetailTow.setVisibility(View.GONE);
        }
        if (isItFromHistory || !mHomeActivity.isLoggedInDriver() || !isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
            //            Hide start job button for history job detail
            mButtonStartJob.setVisibility(View.GONE);
            if (mCurrentJob.getReportAttachment() != null) {
                mTextDisablementAddVehicleReport.setText(getString(R.string.job_details_view_photos));
                mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_blue));
                mTextDisablementAddVehicleReport.setEnabled(true);
            } else {
                mTextDisablementAddVehicleReport.setText(getString(R.string.not_available));
                mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_black));
                mTextDisablementAddVehicleReport.setEnabled(false);
            }
            if (mCurrentJob.getDisablementLocation().getSignature() != null) {
                mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
                mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                mTextDisablementGetSignature.setEnabled(true);
            } else {
                mTextDisablementGetSignature.setText(getString(R.string.not_available));
                mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_black));
                mTextDisablementGetSignature.setEnabled(false);
            }

            if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
                mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
                mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                mTextTowDestinationGetSignature.setEnabled(true);
            } else {
                mTextTowDestinationGetSignature.setText(getString(R.string.not_available));
                mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_black));
                mTextTowDestinationGetSignature.setEnabled(false);
            }
            mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
            //            Get First and Last Name of the user based on driver id.
            if (mCurrentJob.getCurrentStatus() != null
                    && mCurrentJob.getCurrentStatus().getStatusCode() != null) {
                switch (mCurrentJob.getCurrentStatus().getStatusCode()) {
                    case NccConstants.JOB_STATUS_OFFERED:
                        mIncludeEtaDispatcher.setVisibility(View.VISIBLE);
                        mViewEta.setVisibility(View.VISIBLE);
                        mTextServiceDriver.setVisibility(View.GONE);
                        mTextServiceDriverDescription.setVisibility(View.GONE);
                        mViewBorderDriver.setVisibility(View.GONE);
                        mTextServiceStatus.setVisibility(View.GONE);
                        mTextServiceStatusDescription.setVisibility(View.GONE);
                        mViewBorderStatus.setVisibility(View.GONE);
//                        mImageServiceInfo.setVisibility(View.GONE);
//                        mTextServiceUpdatedInfo.setVisibility(View.GONE);
                        mTextServiceEta.setVisibility(View.GONE);
                        mTextServiceQuotedEtaDescription.setVisibility(View.GONE);
                        mViewBorderEta.setVisibility(View.GONE);
                        mTextDisablementVehicleReport.setVisibility(View.GONE);
                        mTextDisablementAddVehicleReport.setVisibility(View.GONE);
                        mViewBorderVehicleReport.setVisibility(View.GONE);
                        mTextDisablementSignature.setVisibility(View.GONE);
                        mTextDisablementGetSignature.setVisibility(View.GONE);
                        mTextTowDestinationSignature.setVisibility(View.GONE);
                        mTextTowDestinationGetSignature.setVisibility(View.GONE);
                        mTextVehicleVin.setVisibility(View.GONE);
                        mTextVehicleVinDescription.setVisibility(View.GONE);
                        mViewBorderVehicleVin.setVisibility(View.GONE);
                        mViewBorderWithVehicle.setVisibility(View.GONE);
                        //mViewBorderTowDestinationNightDropOff.setVisibility(View.GONE);
//                        mHomeActivity.jobOptionsVisible(false);

                        toggleAcceptDeclineEtaLayout(true);

                        break;
                    case NccConstants.JOB_STATUS_ACCEPTED:
                    case NccConstants.JOB_STATUS_AWARDED:
                    case NccConstants.JOB_STATUS_DECLINED:
                    case NccConstants.JOB_STATUS_DENIED:
                    case NccConstants.JOB_STATUS_EXPIRED:
                        toggleAcceptDeclineEtaLayout(false);
                        mIncludeEtaDispatcher.setVisibility(View.GONE);
                        mViewEta.setVisibility(View.VISIBLE);
                        mTextServiceDriver.setVisibility(View.GONE);
                        mTextServiceDriverDescription.setVisibility(View.GONE);
                        mViewBorderDriver.setVisibility(View.GONE);
                        mTextServiceStatus.setVisibility(View.GONE);
                        mTextServiceStatusDescription.setVisibility(View.GONE);
                        mViewBorderStatus.setVisibility(View.GONE);
//                        mImageServiceInfo.setVisibility(View.GONE);
//                        mTextServiceUpdatedInfo.setVisibility(View.GONE);
                        mTextServiceEta.setVisibility(View.GONE);
                        mTextServiceQuotedEtaDescription.setVisibility(View.GONE);
                        mViewBorderEta.setVisibility(View.GONE);
                        mTextDisablementVehicleReport.setVisibility(View.GONE);
                        mTextDisablementAddVehicleReport.setVisibility(View.GONE);
                        mViewBorderVehicleReport.setVisibility(View.GONE);
                        mTextDisablementSignature.setVisibility(View.GONE);
                        mTextDisablementGetSignature.setVisibility(View.GONE);
                        mTextTowDestinationSignature.setVisibility(View.GONE);
                        mTextTowDestinationGetSignature.setVisibility(View.GONE);
                        mTextVehicleVin.setVisibility(View.GONE);
                        mTextVehicleVinDescription.setVisibility(View.GONE);
                        mViewBorderVehicleVin.setVisibility(View.GONE);
                        mViewBorderWithVehicle.setVisibility(View.GONE);
                        //mViewBorderTowDestinationNightDropOff.setVisibility(View.GONE);
//                        mHomeActivity.jobOptionsVisible(false);
                        break;
                    case NccConstants.JOB_STATUS_UNASSIGNED:
                        mIncludeEtaDispatcher.setVisibility(View.GONE);
                        mViewEta.setVisibility(View.GONE);
//                        mHomeActivity.jobOptionsVisible(false);
                        mTextServiceStatus.setVisibility(View.VISIBLE);
                        mTextServiceStatusDescription.setVisibility(View.VISIBLE);
                        mViewBorderStatus.setVisibility(View.VISIBLE);
                        mTextServiceDriver.setVisibility(View.VISIBLE);
                        mTextServiceDriverDescription.setVisibility(View.VISIBLE);
                        mViewBorderDriver.setVisibility(View.VISIBLE);
                        mViewBorderSignature.setVisibility(View.VISIBLE);
                        mTextServiceDriverDescription.setText(getString(R.string.label_assign_driver));
                        mTextServiceStatusDescription.setTextColor(getResources().getColor(R.color.jobdetail_cross_color));
                        mTextServiceStatusDescription.setEnabled(false);
                        mTextServiceStatusDescription.setText(Utils.getDisplayStatusText(mHomeActivity, getString(R.string.unassigned_text)));
                        mTextDisablementAddressDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
                        mTextDisablementAddressDescription.setEnabled(true);
                        mTextTowDestinationAddressDescription.setTextColor(getResources().getColor(R.color.ncc_black));
                        mTextTowDestinationAddressDescription.setEnabled(false);
                        mTextServiceQuotedEtaDescription.setTextColor(getResources().getColor(R.color.ncc_black));
                        mTextServiceQuotedEtaDescription.setEnabled(false);
                        mViewBorderWithVehicle.setVisibility(View.GONE);
                        mTextDisablementVehicleReport.setVisibility(View.GONE);
                        mTextDisablementAddVehicleReport.setVisibility(View.GONE);
                        mViewBorderVehicleReport.setVisibility(View.GONE);
                        mTextDisablementSignature.setVisibility(View.GONE);
                        mTextDisablementGetSignature.setVisibility(View.GONE);
                        //mViewBorderTowDestinationNightDropOff.setVisibility(View.GONE);
                        mTextTowDestinationSignature.setVisibility(View.GONE);
                        mTextTowDestinationGetSignature.setVisibility(View.GONE);
                        mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_black));
                        break;
                    default:
                        mIncludeEtaDispatcher.setVisibility(View.GONE);
                        mViewEta.setVisibility(View.GONE);
                        if (isItFromHistory) {
//                            mHomeActivity.jobOptionsVisible(false);
                        } else {
//                            mHomeActivity.jobOptionsVisible(true);
                        }

                }
            }
        } else {
            if (mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getSignature() != null) {
                mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
            } else {
                mTextDisablementGetSignature.setText(getString(R.string.job_details_get_signature));

            }
            if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
                mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
            } else {
                mTextTowDestinationGetSignature.setText(getString(R.string.job_details_get_signature));
            }
            //            Make Start Job button based on the job getDisplayStatusText
//            mTextDisablementGetSignature.setText(getString(R.string.job_details_get_signature));
//            mTextTowDestinationGetSignature.setText(getString(R.string.job_details_get_signature));
            mButtonStartJob.setVisibility(View.VISIBLE);
            if (mCurrentJob.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(JOB_STATUS_ASSIGNED)) {
                mButtonStartJob.setEnabled(true);
                mButtonStartJob.setText(getString(R.string.assigned_button_start_job));
                mButtonStartJob.setTextColor(getResources().getColor(R.color.ncc_white));
//                mButtonStartJob.setBackgroundResource(R.color.jobdetail_background_button_green);
                setButtonColor(mButtonStartJob, R.color.jobdetail_background_button_green);
            } else if (mCurrentJob.getCurrentStatus()
                    .getStatusCode()
                    .equalsIgnoreCase(JOB_STATUS_ON_SCENE)) {
                if (!Utils.isTow(mCurrentJob.getServiceTypeCode())) {
                    mButtonStartJob.setEnabled(true);
                    mButtonStartJob.setText(getString(R.string.assigned_button_job_complete));
                    mButtonStartJob.setTextColor(getResources().getColor(R.color.ncc_white));
//                    mButtonStartJob.setBackgroundResource(R.color.jobdetail_background_button_green);
                    setButtonColor(mButtonStartJob, R.color.jobdetail_background_button_green);
                } else {
                    mButtonStartJob.setEnabled(false);
                    mButtonStartJob.setText(getString(R.string.assigned_button_job_complete));
                    mButtonStartJob.setTextColor(
                            getResources().getColor(R.color.jobdetail_button_text_color));
//                    mButtonStartJob.setBackgroundResource(R.color.jobdetail_background_text_color);
                    setButtonColor(mButtonStartJob, R.color.jobdetail_background_text_color);
                }
            } else if (mCurrentJob.getCurrentStatus()
                    .getStatusCode()
                    .equals(JOB_STATUS_DESTINATION_ARRIVAL)) {
                mButtonStartJob.setEnabled(true);
                mButtonStartJob.setText(getString(R.string.assigned_button_job_complete));
                mButtonStartJob.setTextColor(getResources().getColor(R.color.ncc_white));
//                mButtonStartJob.setBackgroundResource(R.color.jobdetail_background_button_green);
                setButtonColor(mButtonStartJob, R.color.jobdetail_background_button_green);
            } else if (mCurrentJob.getCurrentStatus()
                    .getStatusCode()
                    .equals(JOB_STATUS_JOB_COMPLETED)) {
                //                showJobCompleteDialog();
                mButtonStartJob.setVisibility(View.GONE);
            } else {
                mButtonStartJob.setEnabled(false);
                mButtonStartJob.setText(getString(R.string.assigned_button_job_complete));
                mButtonStartJob.setTextColor(
                        getResources().getColor(R.color.jobdetail_button_text_color));
//                mButtonStartJob.setBackgroundResource(R.color.jobdetail_background_text_color);
                setButtonColor(mButtonStartJob, R.color.jobdetail_background_text_color);
            }
        }

        if (isItFromHistory) {
            setHistoryView();
        }
        // String statusText = Utils.getDisplayStatusText(getActivity(), mCurrentJob.getCurrentStatus().getStatusCode());
//        if (statusText.toLowerCase().contains("pending") || statusText.toLowerCase().contains("goa") || statusText.toLowerCase().contains("unsuccessful") || statusText.toLowerCase().contains("cancel")) {


        if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
            enableClicks();
        }

        if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            setEnableForTextView(mTextServiceDriverDescription);
        } else {
            setDisableForTextView(mTextServiceDriverDescription, !(mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getIsPending()));
        }

        if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getIsPending()) {
            disableClicks(true);
        } else if (Utils.isUndefinedStatus(mCurrentJob.getCurrentStatus().getStatusCode())) {
            disableClicks(false);
        } else if (mCurrentJob.getCurrentStatus().getIsJobOnHold() != null && mCurrentJob.getCurrentStatus().getIsJobOnHold()) {

            mButtonStartJob.setEnabled(false);
            mButtonStartJob.setTextColor(
                    getResources().getColor(R.color.jobdetail_button_text_color));
            setButtonColor(mButtonStartJob, R.color.jobdetail_background_text_color);
            mTextServiceStatusDescription.setText(getString(R.string.status_on_hold));
        }


        setNotesCallMoreViews();
        setTopInfoLayout();
        setQuotedEtaDateTime();
        setVinInfo();
        setCustomerNameView();
        setDisablementTowAddressView();

        setMarkAsCompletedButtonView();

        setOfferedAcceptedBottomEtaView();
        setVehicleReportView();
        setDisablementSignatureView();
        setTowSignatureView();

        if (!TextUtils.isEmpty(mCurrentJob.getDispatchSource()) && JOB_DISPATCH_SOURCE_ASM_VRM.equalsIgnoreCase(mCurrentJob.getDispatchSource())) {
            updateASMVRMUi();
        }

    }


    private void updateASMVRMUi() {

        //important notes
        mConstraintNotes.setVisibility(View.VISIBLE);
        if (mCurrentJob.getServiceTypeCode().toUpperCase().startsWith(NccConstants.JOB_SERVICE_TYPE_ASM)) {
            mConstraintNotes.setBackgroundColor(ContextCompat.getColor(mHomeActivity, R.color.asm_background));
            mImageStatusInfoConstraintNotes.setImageDrawable(getResources().getDrawable(R.drawable.ic_ico_activejobs_red));
        } else {
            mConstraintNotes.setBackgroundColor(ContextCompat.getColor(mHomeActivity, R.color.jobdetail_notes));
            mImageStatusInfoConstraintNotes.setImageDrawable(getResources().getDrawable(R.drawable.ic_ico_activejobs_alertyellow));
        }
        //dissablement rename
//            mTextDisablement.setText(getString(R.string.accident_scene_title));


        mTextServiceReasonDescription.setText(TextUtils.isEmpty(mCurrentJob.getCallReason()) ? getString(R.string.accident_title)
                : mCurrentJob.getCallReason());

        mTextDisablementGarage.setVisibility(View.GONE);
        mTextDisablementGarageDescription.setVisibility(View.GONE);
        mViewBorderGarage.setVisibility(View.GONE);

        mTextTowGarage.setVisibility(View.GONE);
        mTextTowGarageDescription.setVisibility(View.GONE);
        mViewTowBorderGarage.setVisibility(View.GONE);

        //carrier
        mTextVehicleCarrier.setVisibility(View.VISIBLE);
        mTextVehicleCarrierDescription.setVisibility(View.VISIBLE);
        mViewBorderCarrierCoverage.setVisibility(View.VISIBLE);
        mTextVehicleCarrierDescription.setText(getString(R.string.not_available));
        if (!TextUtils.isEmpty(mCurrentJob.getClientName())) {
            mTextVehicleCarrierDescription.setText(mCurrentJob.getClientName());
        }

        //claim
        mTextVehicleClaimNumber.setVisibility(View.VISIBLE);
        mTextVehicleClaimNumDescription.setVisibility(View.VISIBLE);
        mViewBorderClaimNumber.setVisibility(View.VISIBLE);
        mTextVehicleClaimNumDescription.setText(getString(R.string.not_available));
        if (!TextUtils.isEmpty(mCurrentJob.getClaimNumber())) {
            mTextVehicleClaimNumDescription.setText(mCurrentJob.getClaimNumber());
        } else if (!TextUtils.isEmpty(mCurrentJob.getPolicyNumber())) {
            mTextVehicleClaimNumDescription.setText(mCurrentJob.getPolicyNumber());
        }

        mTextStockLot.setVisibility(View.VISIBLE);
        mTextStockLotDescription.setVisibility(View.VISIBLE);
        mViewBorderTowDestinationStorage.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(mCurrentJob.getTowLocation().getStockLotNumber())) {
            mTextStockLotDescription.setText(mCurrentJob.getTowLocation().getStockLotNumber());
        } else {
            mTextStockLotDescription.setText(getString(R.string.not_available));

        }
        mViewBorderStockLot.setVisibility(View.GONE);
        if (mTextTowDestinationSignature.getVisibility() == View.VISIBLE) {
            mViewBorderStockLot.setVisibility(View.VISIBLE);
        }

    }

    private void setJobId() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            mTextJobId.setVisibility(View.VISIBLE);
            if (mCurrentJob != null || mCurrentJob.getDispatchId() != null) {
                mTextJobId.setText("Job #" + UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId()));
                mTextJobId.setTextColor(getResources().getColor(R.color.ncc_blue));
            }
        } else {
            mTextJobId.setVisibility(View.GONE);
        }

    }

    private void setDisablementSignatureView() {
        mTextDisablementSignature.setVisibility(View.GONE);
        mTextDisablementGetSignature.setVisibility(View.GONE);
//        mViewBorderSignature.setVisibility(View.GONE);

        mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextDisablementGetSignature.setEnabled(false);

        if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getStatusCode() != null && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mTextDisablementSignature.setVisibility(View.VISIBLE);
            mTextDisablementGetSignature.setVisibility(View.VISIBLE);
            mViewBorderSignature.setVisibility(View.VISIBLE);

            if (isMyJob()) {

                if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                    mTextDisablementGetSignature.setEnabled(true);
                    if (mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getSignature() != null) {
                        mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
                    } else {
                        mTextDisablementGetSignature.setText(getString(R.string.job_details_get_signature));
                    }
                } else if (NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {

                    mViewBorderMiles.setVisibility(View.GONE);
                    if (mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getSignature() != null) {
                        mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                        mTextDisablementGetSignature.setEnabled(true);
                        mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
                    } else {
                        mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_black));
                        mTextDisablementGetSignature.setEnabled(false);
                        mTextDisablementGetSignature.setText(getString(R.string.not_available));
                    }
                }


            } else {
                if (mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getSignature() != null) {
                    mTextDisablementGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                    mTextDisablementGetSignature.setEnabled(true);
                    mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
                } else {
                    mTextDisablementGetSignature.setText(getString(R.string.not_available));
                }
            }


        } else if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getIsPending() != null && mCurrentJob.getCurrentStatus().getIsPending()) {
            mViewBorderTowDestinationStorage.setVisibility(View.VISIBLE);
            mTextDisablementSignature.setVisibility(View.VISIBLE);
            mTextDisablementGetSignature.setVisibility(View.VISIBLE);
            mViewBorderSignature.setVisibility(View.VISIBLE);
            mViewBorderWithVehicle.setVisibility(View.VISIBLE);
            if (mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getSignature() != null) {
                mTextDisablementGetSignature.setText(getString(R.string.job_details_view_signature));
            } else {
                mTextDisablementGetSignature.setText(isMyJob() ? getString(R.string.job_details_get_signature) : getString(R.string.not_available));
            }
        }


    }

    private void setTowSignatureView() {
        mTextTowDestinationSignature.setVisibility(View.GONE);
        mTextTowDestinationGetSignature.setVisibility(View.GONE);
//        mViewBorderTowDestinationSignature.setVisibility(View.GONE);

        mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextTowDestinationGetSignature.setEnabled(false);

        if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getStatusCode() != null && mCurrentJob.getServiceTypeCode() != null && Utils.isTow(mCurrentJob.getServiceTypeCode())) {
            if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mTextTowDestinationSignature.setVisibility(View.VISIBLE);
                mTextTowDestinationGetSignature.setVisibility(View.VISIBLE);
//                mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);

                if (isMyJob()) {

                    if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                        mTextTowDestinationGetSignature.setEnabled(true);
                        if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
                            mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
                        } else {
                            mTextTowDestinationGetSignature.setText(getString(R.string.job_details_get_signature));
                        }
                    } else if (NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {

                        if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
                            mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
                            mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                            mTextTowDestinationGetSignature.setEnabled(true);
                        } else {
                            mTextTowDestinationGetSignature.setText(getString(R.string.not_available));
                            mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_black));
                            mTextTowDestinationGetSignature.setEnabled(false);
                        }
                    }

                } else {
                    if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
                        mTextTowDestinationGetSignature.setTextColor(getResources().getColor(R.color.ncc_blue));
                        mTextTowDestinationGetSignature.setEnabled(true);
                        mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
                    } else {
                        mTextTowDestinationGetSignature.setText(getString(R.string.not_available));
                    }
                }


            } else if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getIsPending() != null && mCurrentJob.getCurrentStatus().getIsPending()) {
                mTextTowDestinationSignature.setVisibility(View.VISIBLE);
                mTextTowDestinationGetSignature.setVisibility(View.VISIBLE);
                mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);

                if (mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getSignature() != null) {
                    mTextTowDestinationGetSignature.setText(getString(R.string.job_details_view_signature));
                } else {
                    mTextTowDestinationGetSignature.setText(isMyJob() ? getString(R.string.job_details_get_signature) : getString(R.string.not_available));
                }
            }
        }
    }

    private void setVehicleReportView() {

        mTextDisablementVehicleReport.setVisibility(View.GONE);
        mTextDisablementAddVehicleReport.setVisibility(View.GONE);
        mViewBorderVehicleReport.setVisibility(View.GONE);
        mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextDisablementAddVehicleReport.setEnabled(false);

        if (mCurrentJob != null && mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getStatusCode() != null && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mTextDisablementVehicleReport.setVisibility(View.VISIBLE);
            mTextDisablementAddVehicleReport.setVisibility(View.VISIBLE);
            mViewBorderVehicleReport.setVisibility(View.VISIBLE);

            if (isMyJob()) {
                if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_blue));
                    mTextDisablementAddVehicleReport.setEnabled(true);
                    if (mCurrentJob.getReportAttachment() != null) {
                        mTextDisablementAddVehicleReport.setText(getString(R.string.job_details_view_photos));
                    } else {
                        mTextDisablementAddVehicleReport.setText(getString(R.string.job_details_add_photos));
                    }
                } else if (NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_blue));
                    if (mCurrentJob.getReportAttachment() != null) {
                        mTextDisablementAddVehicleReport.setEnabled(true);
                        mTextDisablementAddVehicleReport.setText(getString(R.string.job_details_view_photos));
                        mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_blue));
                    } else {
                        mTextDisablementAddVehicleReport.setEnabled(false);
                        mTextDisablementAddVehicleReport.setText(getString(R.string.not_available));
                        mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_black));
                    }
                }

            } else {
                if (mCurrentJob.getReportAttachment() != null) {
                    mTextDisablementAddVehicleReport.setTextColor(getResources().getColor(R.color.ncc_blue));
                    mTextDisablementAddVehicleReport.setEnabled(true);
                    mTextDisablementAddVehicleReport.setText(getString(R.string.job_details_view_photos));
                } else {
                    mTextDisablementAddVehicleReport.setText(getString(R.string.not_available));
                }
            }


        } else if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getIsPending() != null && mCurrentJob.getCurrentStatus().getIsPending()) {
            mTextDisablementVehicleReport.setVisibility(View.VISIBLE);
            mTextDisablementAddVehicleReport.setVisibility(View.VISIBLE);
            mViewBorderVehicleReport.setVisibility(View.VISIBLE);

            if (mCurrentJob.getReportAttachment() != null) {
                mTextDisablementAddVehicleReport.setText(getString(R.string.job_details_view_photos));
            } else {
                mTextDisablementAddVehicleReport.setText(isMyJob() ? getString(R.string.job_details_add_photos) : getString(R.string.not_available));
            }
        }

    }

    private boolean isMyJob() {
        boolean isMyJob = false;
        if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchAssignedToId())) {
            String userId = mPrefs.getString(NccConstants.SIGNIN_USER_ID, "");
            isMyJob = userId.equalsIgnoreCase(mCurrentJob.getDispatchAssignedToId());
        }
        return isMyJob;
    }

    private boolean isMyJob(String userId) {
        boolean isMyJob = false;
        if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchAssignedToId())) {
            isMyJob = userId.equalsIgnoreCase(mCurrentJob.getDispatchAssignedToId());
        }
        return isMyJob;
    }

    private void setNotesCallMoreViews() {
        mHomeActivity.enableDisableNotes(true);
        if (mCurrentJob.getCurrentStatus() != null && mCurrentJob.getCurrentStatus().getIsPending() && mHomeActivity.isLoggedInDriver()) {
            mHomeActivity.enableDisableNotes(false);
        }
        if (NccConstants.IS_CHAT_ENABLED) {
            mHomeActivity.showHideNotes(true);
            if (!isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) && getPresentThread() == null) {
                mHomeActivity.enableDisableNotes(false);
            }
            if (mCurrentJob.getDispatchId() != null) {
                mHomeActivity.setCurrentThreadName(getString(R.string.title_job_id) + UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId()));
                mHomeActivity.setMessageCountForThread();
            }
        }
        mHomeActivity.jobCallVisible(false);
        mHomeActivity.enableDisableCall(true);
        if (mCurrentJob != null && mCurrentJob.getCurrentStatus() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mHomeActivity.jobCallVisible(isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || mCurrentJob.getCurrentStatus().getIsPending() || NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_ACCEPTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()));
            if (NccConstants.JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_ACCEPTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mHomeActivity.enableDisableCall(false);
            }
        }


        mHomeActivity.jobOptionsVisible(false);
        if ((isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()))) {
            mHomeActivity.jobOptionsVisible(true);
        }
    }

    private void setOfferedAcceptedBottomEtaView() {
        if (NccConstants.JOB_STATUS_ACCEPTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mIncludeEtaDispatcher.setVisibility(View.VISIBLE);
            toggleAcceptDeclineEtaLayout(false);
        }
    }

    private void setMarkAsCompletedButtonView() {
        if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mButtonStartJob.setVisibility(View.VISIBLE);
            if ((NccConstants.JOB_STATUS_DESTINATION_ARRIVAL.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) && Utils.isTow(mCurrentJob.getServiceTypeCode())) || (NccConstants.JOB_STATUS_ON_SCENE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) && !Utils.isTow(mCurrentJob.getServiceTypeCode()))) {
                mButtonStartJob.setEnabled(true);
                mButtonStartJob.setText(getString(R.string.assigned_button_job_complete));
                mButtonStartJob.setTextColor(getResources().getColor(R.color.ncc_white));
                setButtonColor(mButtonStartJob, R.color.jobdetail_background_button_green);
            } else if (NccConstants.JOB_STATUS_ASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mButtonStartJob.setEnabled(true);
                mButtonStartJob.setText(getString(R.string.assigned_button_start_job));
                mButtonStartJob.setTextColor(getResources().getColor(R.color.ncc_white));
                setButtonColor(mButtonStartJob, R.color.jobdetail_background_button_green);
            } else {
                mButtonStartJob.setEnabled(false);
                mButtonStartJob.setText(getString(R.string.assigned_button_job_complete));
                mButtonStartJob.setTextColor(
                        getResources().getColor(R.color.jobdetail_button_text_color));
                setButtonColor(mButtonStartJob, R.color.jobdetail_background_text_color);
            }
        } else {
            mButtonStartJob.setVisibility(View.GONE);
        }
    }

    private void setDisablementTowAddressView() {
        if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_UNASSIGNED)
                || mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_OFFERED)) {
            setEnableForTextView(mTextDisablementAddressDescription);
            setEnableForTextView(mTextTowDestinationAddressDescription);
        } else {
            setDisableForTextView(mTextDisablementAddressDescription, true);
            setDisableForTextView(mTextTowDestinationAddressDescription, true);
        }
    }

    private void setCustomerNameView() {
        if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mTextCustomer.setVisibility(View.VISIBLE);
            mTextCustomerDescription.setVisibility(View.VISIBLE);
            mViewBorderCustomer.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(mCustomerName)) {
                getCustomerInfo();
            } else {
                setCustomerName(mCustomerName);
            }
        } else {
            mTextCustomer.setVisibility(View.GONE);
            mTextCustomerDescription.setVisibility(View.GONE);
            mViewBorderCustomer.setVisibility(View.GONE);
        }
    }

    private void setQuotedEtaDateTime() {
        if (!TextUtils.isEmpty(mCurrentJob.getEtaDateTime())) {
            try {
                String dateTime;
                if (isUpdateEta()) {
                    dateTime = DateTimeUtils.getEtaDateTime(mCurrentJob.getEtaDateTime(), mCurrentJob.getQuotedEta(), false, mCurrentJob.getDisablementLocation().getTimeZone());

                    mTextServiceQuotedEtaDescription.setTextColor(
                            getResources().getColor(R.color.ncc_blue));
                    mTextServiceQuotedEtaDescription.setEnabled(true);

                } else {
                    mTextServiceQuotedEtaDescription.setTextColor(
                            getResources().getColor(R.color.ncc_black));
                    mTextServiceQuotedEtaDescription.setEnabled(false);
                    dateTime = DateTimeUtils.getEtaDateTime(mCurrentJob.getEtaDateTime(), mCurrentJob.getTotalEtaInMinutes(), false, mCurrentJob.getDisablementLocation().getTimeZone());
                }
                if (dateTime != null && dateTime.contains("am")) {
                    dateTime = dateTime.replace("am", "AM");
                }
                if (dateTime != null && dateTime.contains("pm")) {
                    dateTime = dateTime.replace("pm", "PM");
                }
                mTextServiceQuotedEtaDescription.setText(dateTime);
            } catch (ParseException e) {
                e.printStackTrace();
                mTextServiceQuotedEtaDescription.setText(R.string.not_available);
                mTextServiceQuotedEtaDescription.setTextColor(
                        getResources().getColor(R.color.ncc_black));
            }
        } else {
            if (!TextUtils.isEmpty(mCurrentJob.getAcceptedDateTime())) {
                try {
                    String dateTime;
                    if (isUpdateEta()) {
                        dateTime = DateTimeUtils.getEtaDateTime(mCurrentJob.getAcceptedDateTime(), mCurrentJob.getQuotedEta(), true, mCurrentJob.getDisablementLocation().getTimeZone());
                        mTextServiceQuotedEtaDescription.setTextColor(
                                getResources().getColor(R.color.ncc_blue));
                        mTextServiceQuotedEtaDescription.setEnabled(true);

                    } else {
                        mTextServiceQuotedEtaDescription.setTextColor(
                                getResources().getColor(R.color.ncc_black));
                        mTextServiceQuotedEtaDescription.setEnabled(false);
                        dateTime = DateTimeUtils.getEtaDateTime(mCurrentJob.getAcceptedDateTime(), mCurrentJob.getTotalEtaInMinutes(), true, mCurrentJob.getDisablementLocation().getTimeZone());
                    }
                    if (dateTime != null && dateTime.contains("am")) {
                        dateTime = dateTime.replace("am", "AM");
                    }
                    if (dateTime != null && dateTime.contains("pm")) {
                        dateTime = dateTime.replace("pm", "PM");
                    }
                    mTextServiceQuotedEtaDescription.setText(dateTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                    mTextServiceQuotedEtaDescription.setText(R.string.not_available);
                    mTextServiceQuotedEtaDescription.setTextColor(
                            getResources().getColor(R.color.ncc_black));
                }
            } else {
                mTextServiceQuotedEtaDescription.setText(R.string.not_available);
                mTextServiceQuotedEtaDescription.setTextColor(
                        getResources().getColor(R.color.ncc_black));
            }
        }
    }

    private void setVinInfo() {
        mTextVehicleVinDescription.setEnabled(false);
        mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextVehicleVinNumberHint.setVisibility(View.GONE);
        if (mCurrentJob.getVinCaptureRequired()) {

            String text = mHomeActivity.getString(R.string.job_detail_text_vin_hidden);
            String vinNum = mCurrentJob.getVehicle().getVin();
            StringBuilder builder = new StringBuilder(text);

            if (!TextUtils.isEmpty(mCurrentJob.getVehicle().getVin())) {
                builder.append(vinNum.substring(vinNum.length() - 4));
                mTextVehicleVinNumberHint.setText(builder.toString());
                mTextVehicleVinNumberHint.setVisibility(View.VISIBLE);
            } else {
                mTextVehicleVinNumberHint.setVisibility(View.GONE);
            }

            mTextVehicleVinDescription.setText(getString(R.string.job_detail_text_scan_vin));
            if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()) && isMyJob()) {
                mTextVehicleVinDescription.setEnabled(true);
                mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
                if (mCurrentJob.getCapturedVinInfo() != null && !TextUtils.isEmpty(mCurrentJob.getCapturedVinInfo().getVin())) {
                    mTextVehicleVinDescription.setText(mCurrentJob.getCapturedVinInfo().getVin());
                    mTextVehicleVinNumberHint.setVisibility(View.GONE);
                } else if (!TextUtils.isEmpty(mCurrentJob.getVehicle().getVin())) {
                    mTextVehicleVinNumberHint.setText(builder.toString());
                    mTextVehicleVinNumberHint.setVisibility(View.VISIBLE);
                }
            } else if (mCurrentJob.getCapturedVinInfo() != null && !TextUtils.isEmpty(mCurrentJob.getCapturedVinInfo().getVin())) {
                mTextVehicleVinDescription.setText(mCurrentJob.getCapturedVinInfo().getVin());
                mTextVehicleVinNumberHint.setVisibility(View.GONE);
            } else {
                mTextVehicleVinNumberHint.setVisibility(View.GONE);
            }
        } else if (!TextUtils.isEmpty(mCurrentJob.getVehicle().getVin())) {
            mTextVehicleVinDescription.setText(mCurrentJob.getVehicle().getVin());
        } else {
            mTextVehicleVinDescription.setText(getString(R.string.not_available));
        }
    }

    private void setTopInfoLayout() {
        String statusText = Utils.getDisplayStatusText(mHomeActivity, mCurrentJob.getCurrentStatus().getStatusCode());
        if ((mCurrentJob.getCurrentStatus().getIsPending() != null && mCurrentJob.getCurrentStatus().getIsPending()) ||
                Utils.isUndefinedStatus(mCurrentJob.getCurrentStatus().getStatusCode()) || (mCurrentJob.getCurrentStatus().getIsJobOnHold() != null
                && mCurrentJob.getCurrentStatus().getIsJobOnHold()) || NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                || NccConstants.JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                || NccConstants.JOB_STATUS_DECLINED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                || NccConstants.JOB_STATUS_DENIED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mConstraintStatusInfo.setVisibility(View.VISIBLE);
            GradientDrawable drawableConstraint = (GradientDrawable) mConstraintStatusInfo.getBackground();
            drawableConstraint.setColor(getResources().getColor(R.color.jobdetail_status_info_eta_background_green));
            mTextJobDetailInfoStatus.setTextColor(getResources().getColor(R.color.jobdetail_pending_color));
            if (mCurrentJob.getCurrentStatus().getIsPending() != null && mCurrentJob.getCurrentStatus().getIsPending()) {
                if (mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)) {
                    statusText = statusText + "...";
                } else {
                    statusText = getString(R.string.text_pending) + " " + statusText + "...";
                }
            } else {
                if (NccConstants.JOB_STATUS_JOB_COMPLETED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    drawableConstraint.setColor(getResources().getColor(R.color.jobdetail_status_info_completed_background_green));
                    mTextJobDetailInfoStatus.setTextColor(getResources().getColor(R.color.jobdetail_status_info_completed_text_green));
                } else if (NccConstants.JOB_STATUS_CANCELLED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                        || NccConstants.JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                        || NccConstants.JOB_STATUS_DECLINED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                        || NccConstants.JOB_STATUS_DENIED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    drawableConstraint.setColor(getResources().getColor(R.color.jobdetail_status_info_cancel_background_green));
                    mTextJobDetailInfoStatus.setTextColor(getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
                }
            }

            if (NccConstants.JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mTextJobDetailInfoStatus.setText(getResources().getString(R.string.offer_expired));
            } else if (NccConstants.JOB_STATUS_DENIED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mTextJobDetailInfoStatus.setText(getResources().getString(R.string.offer_denied));
            } else if (NccConstants.JOB_STATUS_DECLINED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                mTextJobDetailInfoStatus.setText(getResources().getString(R.string.offer_declined));
            } else {
                mTextJobDetailInfoStatus.setText(statusText);
            }

            if (mCurrentJob.getCurrentStatus().getUserName() != null) {
                try {
                    StringBuilder jobDesc = new StringBuilder();
                    if (NccConstants.JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())
                            || NccConstants.JOB_STATUS_DENIED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        jobDesc.append(getResources().getString(R.string.note_text_agero));
                    } else {
                        jobDesc.append(Utils.toCamelCase(mCurrentJob.getCurrentStatus().getUserName()));
                    }
                    jobDesc.append(" ");
                    jobDesc.append(getString(R.string.dot));
                    jobDesc.append(" ");
                    jobDesc.append(DateTimeUtils.getDisplayTimeForAlertDetail(
                            DateTimeUtils.parse(
                                    mCurrentJob.getCurrentStatus().getServerTimeUtc()), mCurrentJob.getDisablementLocation().getTimeZone()));
                    mTextJobDetailInfoDescription.setText(jobDesc);
                    mTextJobDetailInfoDescription.setVisibility(View.VISIBLE);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            if (mCurrentJob.getCurrentStatus().getIsJobOnHold() != null && mCurrentJob.getCurrentStatus().getIsJobOnHold()) {
                mTextJobDetailInfoStatus.setText(getString(R.string.job_status_job_on_hold));
                mTextJobDetailInfoDescription.setText(getString(R.string.job_details_on_hold_description));
            }
            if ((NccConstants.JOB_STATUS_CANCELLED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_GOA.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_UNSUCCESSFUL.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_DENIED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) && !TextUtils.isEmpty(mCurrentJob.getCancelReasonDescription())) {
                mViewBorderWithVehicle.setVisibility(View.GONE);
                mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);
                mTextJobDetailInfoStatus.setText(statusText + " " + getString(R.string.dot) + " " + mCurrentJob.getCancelReasonDescription());
            }

            if (mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)) {
                mConstraintStatusInfo.setVisibility(View.GONE);
            }
            if (mHomeActivity.isLoggedInDriver() && (NccConstants.JOB_STATUS_CANCELLED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_GOA.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()))) {
                mViewBorderMiles.setVisibility(View.GONE);
            } else {
                mViewBorderMiles.setVisibility(View.VISIBLE);
            }
        } else {
            mConstraintStatusInfo.setVisibility(View.GONE);
            mTextJobDetailInfoStatus.setText(statusText);
        }
    }

    private void setButtonColor(Button button, int cid) {
        GradientDrawable drawableButton = (GradientDrawable) button.getBackground();
        drawableButton.setColor(getResources().getColor(cid));
    }

    private void enableClicks() {
        mHomeActivity.enableDisableCall(true);
        mHomeActivity.enableDisableNotes(true);
        setEnableForTextView(mTextServiceQuotedEtaDescription);
        setEnableForTextView(mTextServiceStatusDescription);
//        setEnableForTextView(mTextDisablementAddVehicleReport);
//        setEnableForTextView(mTextDisablementGetSignature);
//        setEnableForTextView(mTextTowDestinationGetSignature);
        setEnableForTextView(mTextVehicleVinDescription);

    }

    private void setEnableForTextView(TextView mTextVehicleVinDescription) {
        mTextVehicleVinDescription.setTextColor(
                getResources().getColor(R.color.ncc_blue));
        mTextVehicleVinDescription.setEnabled(true);

    }

    private void disableClicks(boolean isPending) {
        if (isPending) {
            setDisableForTextView(mTextServiceStatusDescription, false);
            setDisableForTextView(mTextServiceQuotedEtaDescription, false);
            setDisableForTextView(mTextDisablementAddVehicleReport, false);
            setDisableForTextView(mTextDisablementGetSignature, false);
            setDisableForTextView(mTextTowDestinationGetSignature, false);
            setDisableForTextView(mTextVehicleVinDescription, false);
//            mHomeActivity.enableDisableCall(false);
            mHomeActivity.enableDisableNotes(!mHomeActivity.isLoggedInDriver());
        } else {
            setDisableForTextView(mTextServiceStatusDescription, true);
            setDisableForTextView(mTextServiceQuotedEtaDescription, true);
            setDisableForTextView(mTextDisablementAddVehicleReport, true);
            setDisableForTextView(mTextDisablementGetSignature, true);
            setDisableForTextView(mTextTowDestinationGetSignature, true);
            setDisableForTextView(mTextVehicleVinDescription, true);
//            mHomeActivity.enableDisableCall(false);
            mHomeActivity.enableDisableNotes(true);
//            mHomeActivity.jobCallVisible(false);
        }
        mButtonStartJob.setEnabled(false);
        mButtonStartJob.setTextColor(
                getResources().getColor(R.color.jobdetail_button_text_color));
        setButtonColor(mButtonStartJob, R.color.jobdetail_background_text_color);
    }

    private void setDisableForTextView(TextView textView, boolean isBlack) {
        if (isBlack) {
            textView.setTextColor(
                    getResources().getColor(R.color.ncc_black));
        } else {
            textView.setTextColor(
                    getResources().getColor(R.color.jobdetail_heading_color));
        }
        textView.setEnabled(false);

    }

    protected boolean isUpdateEta() {

        if (mHomeActivity.isLoggedInDriver()) {
            return mCurrentJob != null && (mCurrentJob.getEtaStatus() == null || !"REQUEST".equalsIgnoreCase(mCurrentJob.getEtaStatus())) &&
                    (mCurrentJob.getEtaExtensionCount() == null || mCurrentJob.getEtaExtensionCount() == 0) &&
                    ((NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) ||
                            NccConstants.JOB_STATUS_ASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) ||
                            NccConstants.JOB_STATUS_EN_ROUTE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()))) &&
                    (!TextUtils.isEmpty(mCurrentJob.getAcceptedDateTime()) || !TextUtils.isEmpty(mCurrentJob.getEtaDateTime()));
        } else {
            return mCurrentJob != null && (mCurrentJob.getEtaStatus() == null || !"REQUEST".equalsIgnoreCase(mCurrentJob.getEtaStatus())) &&
                    ((NccConstants.JOB_STATUS_UNASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) ||
                            NccConstants.JOB_STATUS_ASSIGNED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) ||
                            NccConstants.JOB_STATUS_EN_ROUTE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()))) &&
                    (!TextUtils.isEmpty(mCurrentJob.getAcceptedDateTime()) || !TextUtils.isEmpty(mCurrentJob.getEtaDateTime()));
        }
    }

    private void setHistoryView() {
        if (mCurrentJob.getReportAttachment() == null) {
            mTextDisablementVehicleReport.setVisibility(View.GONE);
            mTextDisablementAddVehicleReport.setVisibility(View.GONE);
            mViewBorderVehicleReport.setVisibility(View.GONE);
        }
        if (mCurrentJob.getDisablementLocation() == null || mCurrentJob.getDisablementLocation().getSignature() == null) {
            mTextDisablementSignature.setVisibility(View.GONE);
            mTextDisablementGetSignature.setVisibility(View.GONE);
//            mViewBorderSignature.setVisibility(View.GONE);
        }
        if (mCurrentJob.getTowLocation() == null || mCurrentJob.getTowLocation().getSignature() == null) {
            mTextTowDestinationSignature.setVisibility(View.GONE);
            mTextTowDestinationGetSignature.setVisibility(View.GONE);
//            mViewBorderTowDestinationSignature.setVisibility(View.GONE);
            mViewBorderTowDestinationStorage.setVisibility(View.GONE);
        }

//        if (mCurrentJob.getCustomerName() == null || TextUtils.isEmpty(mCurrentJob.getCustomerName()) || !mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
//            mTextCustomer.setVisibility(View.GONE);
//            mTextCustomerDescription.setVisibility(View.GONE);
//            mViewBorderCustomer.setVisibility(View.GONE);
//        }
//        if (mCurrentJob.getCoverage() == null || TextUtils.isEmpty(mCurrentJob.getCoverage()) || !mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
//            mTextVehicleCoverage.setVisibility(View.GONE);
//            mTextVehicleCoverageDescription.setVisibility(View.GONE);
//            mViewBorderVehicleCoverage.setVisibility(View.GONE);
//        }
        mViewBorderTowDestinationStorage.setVisibility(View.GONE);
        mHomeActivity.jobOptionsVisible(false);
        mHomeActivity.jobCallVisible(false);
//        mTextCustomerDescription.setEnabled(false);
//        mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextVehicleVinDescription.setEnabled(false);
        mTextVehicleVinDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextDisablementAddressDescription.setEnabled(false);
        mTextDisablementAddressDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextTowDestinationAddressDescription.setEnabled(false);
        mTextTowDestinationAddressDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextServiceQuotedEtaDescription.setEnabled(false);
        mTextServiceQuotedEtaDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        mTextServiceDriverDescription.setEnabled(false);
        mTextServiceDriverDescription.setTextColor(getResources().getColor(R.color.ncc_black));
        if (mCurrentJob.getCurrentStatus() != null
                && mCurrentJob.getCurrentStatus().getStatusCode() != null) {
            switch (mCurrentJob.getCurrentStatus().getStatusCode()) {
                case NccConstants.JOB_STATUS_DECLINED:
                    mTextDisablementVehicleReport.setVisibility(View.GONE);
                    mTextDisablementAddVehicleReport.setVisibility(View.GONE);
                    mViewBorderVehicleReport.setVisibility(View.GONE);
                    mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);
                    mTextServiceStatusDescription.setEnabled(false);
                    mTextServiceStatusDescription.setTextColor(getResources().getColor(R.color.ncc_black));
                    break;
                case NccConstants.JOB_STATUS_EXPIRED:
                    mTextDisablementVehicleReport.setVisibility(View.GONE);
                    mTextDisablementAddVehicleReport.setVisibility(View.GONE);
                    mViewBorderVehicleReport.setVisibility(View.GONE);
                    mTextServiceStatusDescription.setEnabled(false);
                    mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);
                    mTextServiceStatusDescription.setTextColor(getResources().getColor(R.color.ncc_black));
                    break;
                case NccConstants.JOB_STATUS_JOB_COMPLETED:
                    mTextServiceStatusDescription.setText(getResources().getString(R.string.history_text_completed));
                    mTextServiceStatusDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
                    mTextServiceStatusDescription.setEnabled(true);
                    mTextDisablementSignature.setVisibility(View.VISIBLE);
                    mTextDisablementGetSignature.setVisibility(View.VISIBLE);
                    mViewBorderSignature.setVisibility(View.VISIBLE);
                    mTextDisablementVehicleReport.setVisibility(View.VISIBLE);
                    mTextDisablementAddVehicleReport.setVisibility(View.VISIBLE);
                    mViewBorderVehicleReport.setVisibility(View.VISIBLE);
                    mTextTowDestinationSignature.setVisibility(View.VISIBLE);
                    mTextTowDestinationGetSignature.setVisibility(View.VISIBLE);
                    mViewBorderTowDestinationSignature.setVisibility(View.VISIBLE);
                    mViewBorderTowDestinationStorage.setVisibility(View.VISIBLE);
//                    mTextCustomer.setVisibility(View.VISIBLE);
//                    mTextCustomerDescription.setVisibility(View.VISIBLE);
                    mViewBorderCustomer.setVisibility(View.VISIBLE);
                    mViewBorderInvoice.setVisibility(View.VISIBLE);
                    break;
            }
        }


    }

    /**
     * Initialize map.
     */
    public void loadMap() {

        if (isAdded() && isVisible()) {
            if (isItFromHistory) {
                mHomeActivity.updateAnalyticsCurrentScreen(getResources().getString(R.string.label_history_detail));
            } else {
                mHomeActivity.updateAnalyticsCurrentScreen(getResources().getString(R.string.label_jobdetail));
            }

            SupportMapFragment supportMapFragment = new SupportMapFragment();

            getChildFragmentManager().beginTransaction()
                    .add(R.id.frame_map_container, supportMapFragment)
                    .commit();
            supportMapFragment.getMapAsync(this);
        }
    }

    /**
     * To set marker for Facility and draw directions
     */
    private void updateFacilityInMap() {
        if (mMap == null || mFacility == null) {
            if (view != null && isAdded()) {
                view.findViewById(R.id.p1).setVisibility(View.GONE);
                view.findViewById(R.id.job_details_parent1).setVisibility(View.VISIBLE);
                setVisiblityLayout(View.VISIBLE);

            }
            return;
        }

        LatLng facilityLatLng = null;
        LatLng disablementLatLng = null;
        LatLng driverLatLng = null;

        if (mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getLatitude() != null && mCurrentJob.getDisablementLocation().getLongitude() != null) {
            disablementLatLng = new LatLng(mCurrentJob.getDisablementLocation().getLatitude(),
                    mCurrentJob.getDisablementLocation().getLongitude());

            if (mFacilityLineOptions != null) {
                Polyline polyline = mMap.addPolyline(mFacilityLineOptions);
                if ((mFacilityLineOptions.getPattern() == null) || (!mFacilityLineOptions.getPattern()
                        .contains(DASH))) {
                    polyline.setPattern(PATTERN_POLYLINE_DOTTED);
                    polyline.setColor(getResources().getColor(R.color.ncc_gray_666));
                }
            } else {
                DownloadDirections downloadDirectionsTask = new DownloadDirections(
                        new DownloadDirections.DirectionDownloadedListener() {
                            @Override
                            public void onSuccess(PolylineOptions polylineOptions) {
                                if (isAdded()) {
                                    mFacilityLineOptions = polylineOptions;
                                    Polyline polyline = mMap.addPolyline(mFacilityLineOptions);
                                    if ((mFacilityLineOptions.getPattern() == null)
                                            || (!mFacilityLineOptions.getPattern().contains(DASH))) {
                                        polyline.setPattern(PATTERN_POLYLINE_DOTTED);
                                        polyline.setColor(getResources().getColor(R.color.ncc_gray_666));
                                    }
                                }
                            }

                            @Override
                            public void onFailure() {
                                mHomeActivity.showError(
                                        BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(),
                                        new UserError("", "",
                                                getString(R.string.map_error_directions)));
                            }
                        });
                if (mFacility.getAddressList() != null
                        && mFacility.getAddressList().size() > 0
                        && mFacility.getAddressList().get(0).getLatitude() != null
                        && mFacility.getAddressList().get(0).getLongitude() != null
                        && mFacility.getAddressList().get(0).getLatitude() != 0.0
                        && mFacility.getAddressList().get(0).getLongitude() != 0.0) {
                    facilityLatLng = new LatLng(mFacility.getAddressList().get(0).getLatitude(), mFacility.getAddressList().get(0).getLongitude());
                    Disposable dirDisposable = downloadDirectionsTask.execute(Utils.getDirectionsUrl(facilityLatLng, disablementLatLng));
                    disposables.add(dirDisposable);
                }
            }

        }

        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();

        if (disablementLatLng != null) {
            latLngBuilder.include(disablementLatLng);
        }

        if (Utils.isTow(mCurrentJob.getServiceTypeCode()) && mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getLatitude() != null && mCurrentJob.getTowLocation().getLongitude() != null) {
            LatLng towToLatLng = new LatLng(mCurrentJob.getTowLocation().getLatitude(),
                    mCurrentJob.getTowLocation().getLongitude());
            latLngBuilder.include(towToLatLng);
        }


        if (mFacility.getAddressList() != null
                && mFacility.getAddressList().size() > 0
                && mFacility.getAddressList().get(0).getLatitude() != null
                && mFacility.getAddressList().get(0).getLongitude() != null
                && mFacility.getAddressList().get(0).getLatitude() != 0.0
                && mFacility.getAddressList().get(0).getLongitude() != 0.0) {
            facilityLatLng = new LatLng(mFacility.getAddressList().get(0).getLatitude(), mFacility.getAddressList().get(0).getLongitude());
            MarkerOptions facilityMarkerOptions = new MarkerOptions();
            facilityMarkerOptions.position(facilityLatLng);
            facilityMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_facility));
            mMap.addMarker(facilityMarkerOptions);
            latLngBuilder.include(facilityLatLng);
        }

        if (mCurrentLatLng != null && mCurrentLatLng.longitude != 0.0 && mCurrentLatLng.latitude != 0.0 && isMyJob()) {
            latLngBuilder.include(mCurrentLatLng);
        } else if (!isMyJob() && mUserProfile != null && mUserProfile.getLocation() != null && mUserProfile.getLocation().getLatitude() != null && mUserProfile.getLocation().getLatitude() != 0.0 && mUserProfile.getLocation().getLongitude() != null && mUserProfile.getLocation().getLongitude() != 0.0) {
            driverLatLng = new LatLng(mUserProfile.getLocation().getLatitude(), mUserProfile.getLocation().getLongitude());
            latLngBuilder.include(driverLatLng);
        }

        LatLngBounds latLngBounds = latLngBuilder.build();
        float value = getResources().getDimension(R.dimen.job_details_map_height);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(latLngBounds, (int) value / 3);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (view != null && isAdded()) {
                    view.findViewById(R.id.p1).setVisibility(View.GONE);
                    view.findViewById(R.id.job_details_parent1).setVisibility(View.VISIBLE);
                    setVisiblityLayout(View.VISIBLE);

                }
            }
        }, 500);

        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                /**set animated zoom camera into map*/
                if (isAdded() && isVisible() && mMap != null && cu != null) {
                    mMap.animateCamera(cu);
                }
            }
        });
    }

    /**
     * To set marker for Disablement and Tow destination and draw directions
     */
    public void updateMapWithMarkers() {
        if (mMap == null) {
            return;
        }
        //LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        LatLng disablementLatLng = null;
        if (mCurrentJob != null && mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation().getLatitude() != null && mCurrentJob.getDisablementLocation().getLongitude() != null) {
            disablementLatLng = new LatLng(mCurrentJob.getDisablementLocation().getLatitude(),
                    mCurrentJob.getDisablementLocation().getLongitude());
        }

        //        Set marker and draw directions only if job type Tow.
        if (mCurrentJob.getServiceTypeCode() != null && Utils.isTow(mCurrentJob.getServiceTypeCode())) {
            LatLng towToLatLng = null;
            if (mCurrentJob != null && mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation().getLatitude() != null &&
                    mCurrentJob.getTowLocation().getLongitude() != null) {
                towToLatLng = new LatLng(mCurrentJob.getTowLocation().getLatitude(),
                        mCurrentJob.getTowLocation().getLongitude());

                MarkerOptions towToMarkerOptions = new MarkerOptions();
                towToMarkerOptions.position(towToLatLng);
                towToMarkerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_towto));
                mMap.addMarker(towToMarkerOptions);
            }
            //latLngBuilder.include(towToLatLng);


            if (mLineOptions != null) {
                mMap.addPolyline(mLineOptions);
            } else {
                mLineOptions = new PolylineOptions();
                DownloadDirections downloadDirectionsTask =
                        new DownloadDirections(
                                new DownloadDirections.DirectionDownloadedListener() {
                                    @Override
                                    public void onSuccess(PolylineOptions polylineOptions) {
                                        mLineOptions = polylineOptions;
                                        mMap.addPolyline(mLineOptions);
                                    }

                                    @Override
                                    public void onFailure() {
                                        mHomeActivity.showError(
                                                BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(),
                                                new UserError("", "",
                                                        getString(R.string.map_error_directions)));
                                    }
                                });
                if (disablementLatLng != null && towToLatLng != null) {
                    Disposable dirDisposable = downloadDirectionsTask.execute(
                            Utils.getDirectionsUrl(disablementLatLng, towToLatLng));
                    disposables.add(dirDisposable);
                }
            }
        }

        if (disablementLatLng != null) {
            MarkerOptions disablementMarkerOptions = new MarkerOptions();
            disablementMarkerOptions.position(disablementLatLng);
            if (!TextUtils.isEmpty(mCurrentJob.getDispatchSource()) && JOB_DISPATCH_SOURCE_ASM_VRM.equalsIgnoreCase(mCurrentJob.getDispatchSource()) && (mCurrentJob.getServiceTypeCode().toUpperCase().startsWith(NccConstants.JOB_SERVICE_TYPE_ASM))) {
                disablementMarkerOptions.icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.ico_asm_towto));
            } else {
                disablementMarkerOptions.icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_disablement));
            }
            mMap.addMarker(disablementMarkerOptions);
        }

        updateFacilityInMap();
    }

    private void displayPendingStatus(String title) {
        AlertDialogFragment alert = null;
        try {
            alert = AlertDialogFragment.newDialog(Html.fromHtml(title), Html.fromHtml("<font color='#999999'>" + "ETA: "
                            + DateTimeUtils.getEtaDateTime(mCurrentJob.getEtaDateTime(), mCurrentJob.getEtaChangeHistory().get(0).getEtaInMinutes(), true, mCurrentJob.getDisablementLocation().getTimeZone())
                            + " (+" + mCurrentJob.getEtaChangeHistory().get(0).getEtaInMinutes() + "min)" + " Requested by "
                            + Utils.toCamelCase(mCurrentJob.getEtaChangeHistory().get(0).getUserName()) + " "
                            + getString(R.string.dot) + " " + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(mCurrentJob.getEtaChangeHistory().get(0).getServerTimeUtc().trim()), mCurrentJob.getDisablementLocation().getTimeZone()) + "<br><br>"
                            + "Original ETA: " + DateTimeUtils.getEtaDateTime(mCurrentJob.getEtaDateTime(), mCurrentJob.getEtaChangeHistory().get(0).getEtaInMinutes(), false, mCurrentJob.getDisablementLocation().getTimeZone()) + "</font>")
                    , "", Html.fromHtml("<b>" + getString(R.string.ok) + "</b>"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
//                setVisiblityReason(View.VISIBLE);
            }
        });
        alert.show(getFragmentManager().beginTransaction(), EditServiceFragment.class.getClass().getCanonicalName());
    }

    private void updateDeclinedData(Status currentStatus) {
        myRef.child("currentStatus").runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                mutableData.setValue(currentStatus);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {

            }
        });
    }

    public void showCallSelectionBottomSheet() {
        final JobDetailCallBottomSheetDialog jobDetailCallBottomSheetDialog =
                JobDetailCallBottomSheetDialog.getInstance();
        if (isAdded()) {
            jobDetailCallBottomSheetDialog.show(getFragmentManager(), "Call");
        }

        jobDetailCallBottomSheetDialog.setJobDetailCallDialogClickListener(
                new JobDetailCallBottomSheetDialog.CallDialogClickListener() {
                    @Override
                    public void onCustomerClick() {
                        if (isAdded()) {
                            // To update with live data from Job
                            if (mCurrentJob != null && mCurrentJob.getCustomerCallbackNumber() != null) {
                                try {
                                    makePhoneCall(mCurrentJob.getCustomerCallbackNumber());
                                } catch (Exception e) {
                                    mUserError.message = "Cannot make call";
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                }
                            }
                            if (jobDetailCallBottomSheetDialog != null) {
                                jobDetailCallBottomSheetDialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onDispatcherClick() {
                        // To update with live data from Job
                        if (isAdded()) {
                            String phoneNumber = mPrefs.getString(NccConstants.DISPATCHER_NUMBER, "");
                            if (!TextUtils.isEmpty(phoneNumber)) {
                                try {
                                    makePhoneCall(phoneNumber);
                                } catch (Exception e) {
                                    mUserError.message = "Cannot make call";
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                }
                            }

                            if (jobDetailCallBottomSheetDialog != null) {
                                jobDetailCallBottomSheetDialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onAgeroClick() {
                        // To update with live data from Job
                        if (isAdded()) {
                            try {
                                makePhoneCall(getResources().getString(R.string.ncc_agero_phone_number));
                            } catch (Exception e) {
                                mUserError.message = "Cannot make call";
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                            }
                            if (jobDetailCallBottomSheetDialog != null) {
                                jobDetailCallBottomSheetDialog.dismiss();
                            }
                        }
                    }
                });
    }

    public void makePhoneCall(String phoneNumber) {
        selectedNumbertoCall = phoneNumber;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Dexter.isRequestOngoing()) {
                Dexter.checkPermission(phoneCallPermissionListener, Manifest.permission.CALL_PHONE);
            }
        } else {
            Utils.makePhoneCall(getActivity(), selectedNumbertoCall);
        }
    }

    /**
     * To show dialog for permanently denied permission.
     */
    public void showPermissionDeniedDialog() {
        AlertDialogFragment alert = AlertDialogFragment.newOkDialog(
                getResources().getString(R.string.permission_denied_title),
                getResources().getString(R.string.permission_denied_camera_message));
        alert.setListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", getActivity().getPackageName(), null)));
                dialogInterface.dismiss();
            }
        });
        alert.show(getFragmentManager().beginTransaction(),
                JobDetailsFragment.class.getClass().getCanonicalName());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (isAdded()) {
            mMap = googleMap;
            Observable<Location> locationObservable = mHomeActivity.getLocationObservable();


            if (NCCApplication.mLocationData != null) {
                mCurrentLatLng =
                        new LatLng(NCCApplication.mLocationData.getLatitude(), NCCApplication.mLocationData.getLongitude());
                MarkerOptions currentLocationMarkerOptions = new MarkerOptions();
                currentLocationMarkerOptions.position(mCurrentLatLng);
                currentLocationMarkerOptions.icon(
                        BitmapDescriptorFactory.fromResource(R.mipmap.ic_pin_current_location));
                mMap.addMarker(currentLocationMarkerOptions);
            }

            locationObservable.subscribe(location -> {
                if (mMap != null && isAdded() && isVisible()) {
                    mCurrentLatLng =
                            new LatLng(location.getLatitude(), location.getLongitude());
                    MarkerOptions currentLocationMarkerOptions = new MarkerOptions();
                    currentLocationMarkerOptions.position(mCurrentLatLng);
                    currentLocationMarkerOptions.icon(
                            BitmapDescriptorFactory.fromResource(R.mipmap.ic_pin_current_location));
                    mMap.addMarker(currentLocationMarkerOptions);
                }
            });
            if (mCurrentJob != null) {
                updateMapWithMarkers();
            }

        }
    }

    @Override
    public void onCall() {
        if (Utils.isCallSupported(mHomeActivity)) {
            showCallSelectionBottomSheet();
        } else {
            mUserError.message = "Cannot make call";
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private User createUserForProfile(String toUserId, Profile profile) {
        User user = StorageManager.shared().createEntity(User.class);

        final UserWrapper userWrapper1 = UserWrapper.initWithModel(user);


        userWrapper1.getModel().setEntityID(toUserId);
        userWrapper1.getModel().setEmail(profile.getEmail());
        userWrapper1.getModel().setName(profile.getFirstName() + " " + profile.getLastName());
        userWrapper1.getModel().setPhoneNumber(profile.getMobilePhoneNumber());
        userWrapper1.getModel().setAuthenticationType(2);
        userWrapper1.getModel().setLastOnline(new Date());
        userWrapper1.getModel().setLastUpdated(new Date());


        userWrapper1.getModel().update();

        return userWrapper1.getModel();
    }

    private void getUsersList() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        myRef = database.getReference(
                                "Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
                        //myRef.keepSynced(true);
                        myRef.addValueEventListener(mTotalUserValueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private LinkedHashMap<String, Profile> mTotalUserHashMap = new LinkedHashMap<>();
    private ArrayList<Profile> mUsersList = new ArrayList<>();

    ValueEventListener mTotalUserValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            try {
                myRef.removeEventListener(mTotalUserValueEventListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
            hideProgress();
            if (dataSnapshot.hasChildren() && isAdded()) {
                mTotalUserHashMap.clear();
                mUsersList.clear();
                showProgress();
                Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
                for (DataSnapshot data : childList) {
                    try {
                        Profile userProfile = data.getValue(Profile.class);
                        if (userProfile != null && userProfile.getStatus() != null
                                && !userProfile.getStatus().equalsIgnoreCase(USER_STATUS_INACTIVE)
                                && !userProfile.getStatus().equalsIgnoreCase(USER_STATUS_BLOCKED)
                                && !userProfile.getStatus().equalsIgnoreCase(USER_STATUS_DELETED)) {
                            if (isMyJob(data.getKey()) || mHomeActivity.isUserDispatcher(userProfile.getRoles())) {
                                userProfile.setUserId(data.getKey());
                                mUsersList.add(userProfile);
                            }
                            if (mPrefs.getString(NccConstants.SIGNIN_USER_ID, "").equalsIgnoreCase(data.getKey())) {
                                mUserProfile = userProfile;
                            }
                            mTotalUserHashMap.put(data.getKey(), userProfile);
                        }
                    } catch (Exception e) {
                        mHomeActivity.mintLogException(e);
                    }
                }

                if (mUsersList.size() > 0) {
                    hideProgress();
                    String threadName = getString(R.string.title_job_id) + UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId());
                    ChatActivity.isFromJobDetail = true;
                    ChatActivity.toUserName = "";
                    mHomeActivity.push(ChatNewConversationFragment.newInstance(mUsersList, ThreadType.PrivateGroup, threadName), getString(R.string.new_conversation));
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Chat History Total User List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getUsersList();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void createPrivateGroup() {
        if (Utils.isNetworkAvailable()) {
            if (mCurrentJob != null) {
                Thread presentThread = getPresentThread();
                if (presentThread == null && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    getUsersList();
                } else if (!userInThread(presentThread, mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")) && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    if (mUserProfile == null) {
                        mUserProfile = createProfile();
                    }
                    User user = createUserForProfile(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mUserProfile);
                    presentThread.addUser(user);
                    ChatActivity.isFromJobDetail = true;
                    ChatActivity.toUserName = "";
                    InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, presentThread.getEntityID());
                } else {
                    if (presentThread != null) {
                        ChatActivity.isFromJobDetail = true;
                        ChatActivity.toUserName = "";
                        InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, presentThread.getEntityID());
                    } else {

                    }
                }
            }
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    private Thread getPresentThread() {
        String threadName = getString(R.string.title_job_id) + UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId());

        Thread presentThread = null;
        List<Thread> threadList = NM.thread().getThreads(ThreadType.Private, false);
        for (Thread thread :
                threadList) {
            if (threadName.equalsIgnoreCase(thread.getName())) {
                presentThread = thread;
                break;
            }
        }
        return presentThread;
    }

    private Profile createProfile() {

        Profile userProfile = new Profile();
        userProfile.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        userProfile.setEmail(mPrefs.getString(NccConstants.SIGNIN_EMAILADDRESS, ""));
        String[] name = mPrefs.getString(NccConstants.SIGNIN_USER_NAME, "").split(" ");
        userProfile.setFirstName(name[0]);
        if (name.length > 1) {
            userProfile.setLastName(name[1]);
        } else {
            userProfile.setLastName("");
        }
        userProfile.setMobilePhoneNumber("");
        return userProfile;
    }

    private boolean userInThread(Thread presentThread, String userId) {
        boolean isUserPresent = false;
        for (User user :
                presentThread.getUsers()) {
            if (userId.equalsIgnoreCase(user.getEntityID())) {
                isUserPresent = true;
            }
        }
        return isUserPresent;
    }
   /* private void createPublicThread() {
        if(isAdded() && mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchId())) {
            showOrUpdateProgressDialog(getString(co.chatsdk.ui.R.string.add_public_chat_dialog_progress_message));
            final String threadName = UiUtils.getJobIdDisplayFormat(mCurrentJob.getDispatchId());

            NM.publicThread().createPublicThreadWithName(threadName)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((thread, throwable) -> {
                        if(isAdded()) {
                            if (throwable == null) {
                                dismissProgressDialog();

                                ToastHelper.show(getContext(), String.format(getString(co.chatsdk.ui.R.string.public_thread__is_created), threadName));

                                addUsersToPublicThread(thread,users);
                            } else {
                                ChatSDK.logError(throwable);
                                Toast.makeText(BaseJobDetailsFragment.this.getContext(), throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                dismissProgressDialog();
                            }
                        }
                    });

          *//*  NM.thread().createThread(threadName, users)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSuccess(thread -> {
                        if (thread != null) {
                            InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, thread.getEntityID());
                        }
                    }).doOnError(throwable -> ToastHelper.show(mHomeActivity, co.chatsdk.ui.R.string.create_thread_with_users_fail_toast));*//*
        }
    }

    private void addUsersToPublicThread(Thread thread, List<User> users) {

//        NM.thread().addUsersToThread(thread,users).subscribe();
        NM.thread().addUsersToThread(thread, users)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    dismissProgressDialog();
                    if (thread != null) {
                        InterfaceManager.shared().a.startChatActivityForID(mHomeActivity, thread.getEntityID());
                    }
                }, throwable -> {
                    ChatSDK.logError(throwable);
                    dismissProgressDialog();
                });

    }*/

    @Override
    public void onNotes() {
        if (NccConstants.IS_CHAT_ENABLED) {
            createPrivateGroup();
        } else {
            if (mCurrentJob != null && mCurrentJob.getCurrentStatus() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
                if (isItFromHistory) {
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                    eventAction = NccConstants.FirebaseEventAction.VIEW_NOTES;
                } else {
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                    eventAction = NccConstants.FirebaseEventAction.VIEW_NOTES;
                }
                mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                mHomeActivity.push(NotesFragment.newInstance(isItFromHistory, !(JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_REJECTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_DECLINED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())), false, mDispatchNumber, mCurrentJob.getDispatchCreatedDate(), new Gson().toJson(mCurrentJob)), "");
            }
        }
    }

    @Override
    public void onOptions() {

    }

    @OnClick({
            R.id.frame_map_overlay, R.id.button_start_job, R.id.text_service_status_description,
            R.id.text_disablement_add_vehicle_report, R.id.text_disablement_get_signature,
            R.id.text_tow_destination_get_signature, R.id.text_vehicle_vin_description,
            R.id.text_disablement_address_description,
            R.id.text_tow_destination_address_description, R.id.constraint_notes,
            R.id.text_service_quoted_eta_description, R.id.text_service_driver_description,
            R.id.image_eta_increase, R.id.image_eta_decrease, R.id.button_request,
            R.id.image_restrict, R.id.eta_request_lay, R.id.text_customer_description,
            R.id.text_tow_destination_get_contact_number
    })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.frame_map_overlay:
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.VIEW_MAP;
                if (mCurrentJob != null && mFacility != null) {
                    mHomeActivity.push(
                            JobDetailMapFragment.newInstance(mCurrentJob, mFacility, mLineOptions,
                                    mFacilityLineOptions), getString(R.string.title_job_detail_map));
                }
                break;
            case R.id.text_disablement_add_vehicle_report:

//                if (mCurrentJob != null && mCurrentJob.getDispatchId() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
//                    if (mHomeActivity.isLoggedInDriver() && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
//                        mHomeActivity.push(VehicleReportFragment.newInstance(mCurrentJob.getDispatchId(),
//                                true, isItFromHistory), getString(R.string.title_vehicle_condition_report));
//                    } else {
//                        mHomeActivity.push(VehicleReportFragment.newInstance(mCurrentJob.getDispatchId(),
//                                false, isItFromHistory), getString(R.string.title_vehicle_condition_report_history));
//                    }
//                }
                if (isItFromHistory) {
                    eventAction = NccConstants.FirebaseEventAction.VIEW_ATTACHMENTS;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                } else {
                    eventAction = NccConstants.FirebaseEventAction.ADD_ATTACHMENTS;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                }
                if (mCurrentJob != null && mCurrentJob.getDispatchId() != null) {
                    if (isMyJob() && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        mHomeActivity.push(VehicleReportFragment.newInstance(mCurrentJob.getDispatchId(),
                                true, isItFromHistory), getString(R.string.title_vehicle_condition_report));
                    } else {
                        mHomeActivity.push(VehicleReportFragment.newInstance(mCurrentJob.getDispatchId(),
                                false, isItFromHistory), getString(R.string.title_vehicle_condition_report_history));
                    }
                }

                break;
            case R.id.text_vehicle_vin_description:
                if (mCurrentJob != null && mCurrentJob.getDispatchId() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode()) && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    mHomeActivity.push(ScanVinBarCodeFragment.newInstance(mDispatchNumber),
                            getString(R.string.title_scanvin));
                }
                break;
            case R.id.text_disablement_get_signature:
                if (mCurrentJob != null && mCurrentJob.getDispatchId() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    if (SignatureDetailsFragment.mDisablementBitmap != null) {
                        if (!SignatureDetailsFragment.mDisablementBitmap.isRecycled()) {
                            SignatureDetailsFragment.mDisablementBitmap.recycle();
                        }
                        SignatureDetailsFragment.mDisablementBitmap = null;
                    }

                    mHomeActivity.push(SignatureDetailsFragment.newInstance(true, isMyJob() && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()),
                            mDispatchNumber, isItFromHistory), getString(R.string.title_disablement_sign));
                }
                if (isItFromHistory) {
                    eventAction = NccConstants.FirebaseEventAction.VIEW_DISABLEMENT_SIGNATURE;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                } else {
                    eventAction = NccConstants.FirebaseEventAction.ADD_DISABLEMENT_SIGNATURE;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                }
//                mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                break;
            case R.id.text_tow_destination_get_signature:
                if (isItFromHistory) {
                    eventAction = NccConstants.FirebaseEventAction.VIEW_TOW_DEST_SIGNATURE;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                } else {
                    eventAction = NccConstants.FirebaseEventAction.ADD_TOW_DEST_SIGNATURE;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                }
                if (mCurrentJob != null && mCurrentJob.getDispatchId() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    if (SignatureDetailsFragment.mTowBitmap != null) {
                        if (!SignatureDetailsFragment.mTowBitmap.isRecycled()) {
                            SignatureDetailsFragment.mTowBitmap.recycle();
                        }
                        SignatureDetailsFragment.mTowBitmap = null;
                    }
                    mHomeActivity.push(SignatureDetailsFragment.newInstance(false, isMyJob() && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()),
                            mDispatchNumber, isItFromHistory), getString(R.string.title_dropoff_sign));
                }
                break;
            case R.id.text_disablement_address_description:
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.GET_DIRECTIONS;
                if (!isItFromHistory && mCurrentJob.getDisablementLocation() != null && mCurrentJob.getDisablementLocation()
                        .getLatitude() != null &&  mCurrentJob.getDisablementLocation()
                        .getLongitude() != null) {
                    //                    DisablementLocation disablementLocation = mCurrentJob.getDisablementLocation();
                    //                    mHomeActivity.push(EditTowDisablementFragment.newInstance(true,
                    //                            new Gson().toJson(disablementLocation), mUserProfile.getFirstName()),
                    //                            getString(R.string.title_edit_tow));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                            "geo:<lat>,<long>?q=<" + mCurrentJob.getDisablementLocation()
                                    .getLatitude() + ">,<" + mCurrentJob.getDisablementLocation()
                                    .getLongitude() + ">"));
                    startActivity(intent);
                }
                break;
            case R.id.text_tow_destination_address_description:
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.GET_DIRECTIONS;
                if (!isItFromHistory && mCurrentJob.getTowLocation() != null && mCurrentJob.getTowLocation()
                        .getLatitude() != null && mCurrentJob.getTowLocation()
                        .getLongitude() != null) {
                    //                    TowDestination towDestination = mCurrentJob.getTowDestination();
                    //                    mHomeActivity.push(EditTowDisablementFragment.newInstance(false,
                    //                            new Gson().toJson(towDestination), mUserProfile.getFirstName()), getString(R.string.title_edit_tow));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                            "geo:<lat>,<long>?q=<"
                                    + mCurrentJob.getTowLocation().getLatitude()
                                    + ">,<"
                                    + mCurrentJob.getTowLocation().getLongitude()
                                    + ">"));
                    startActivity(intent);
                }
                break;
            case R.id.constraint_notes:
                if (mCurrentJob != null && mCurrentJob.getDispatchId() != null && mCurrentJob.getCurrentStatus() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
                    if (isItFromHistory) {
                        eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                        eventAction = NccConstants.FirebaseEventAction.VIEW_NOTES_TAG;
                    } else {
                        eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                        eventAction = NccConstants.FirebaseEventAction.VIEW_NOTES_TAG;
                    }

                    mHomeActivity.push(NotesFragment.newInstance(isItFromHistory, !(JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_REJECTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_DECLINED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())), true, mDispatchNumber, mCurrentJob.getDispatchCreatedDate(), new Gson().toJson(mCurrentJob)), getString(R.string.title_notes));
                }
                break;
            case R.id.text_service_quoted_eta_description:
//                if (mHomeActivity.isLoggedInDriver()) {
//                    showUpdateETADialog(true);
//                } else {
//                    showUpdateETADialog(false);
//                }
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.UPDATE_ETA;
                if (isUpdateEta() && mUserProfile != null) {
                    mHomeActivity.push(UpdateETAFragment.newInstance(
                            mUserProfile.getFirstName() == null ? " " : mUserProfile.getFirstName(), mDispatchNumber, mCurrentJob.getDisablementLocation().getTimeZone()), getString(R.string.alert_label_update_eta));
                }
                break;
            case R.id.image_eta_decrease:
                mEtaValue = mEtaValue - 5;
                checkEtaValue(mEtaValue);
                break;
            case R.id.image_eta_increase:
                mEtaValue = mEtaValue + 5;
                checkEtaValue(mEtaValue);
                break;
            case R.id.button_request:
//                if ((Utils.isTow(mCurrentJob.getServiceTypeCode()) && mEtaValue > TOW_JOB_INITIAL_ETA) || (!Utils.isTow(mCurrentJob.getServiceTypeCode()) && mEtaValue > OTHER_JOB_ETA)) {
                if (mEtaValue > 60) {
//                    String[] provider = getResources().getStringArray(R.array.array_eta_approval);
//                    displaySingleChoiceAlertDialog(
//                            getResources().getString(R.string.title_eta_requires_approval),
//                            provider, "", getResources().getString(R.string.ok),
//                            BaseJobDetailsFragment.this::onChoiceClicked,
//                            NccConstants.DISPATCHER_REQUEST_JOB);
                    showETAAcceptJobOfferDialog();
                } else {
                    //updateStatus(NccConstants.JOB_STATUS_ACCEPTED, null, null);

                    updateAcceptStatus(mHomeActivity, mAcceptDecline, mCurrentJob, mDispatchNumber, mUserError, mEtaValue, currentStatusForUpdate, "");
                }
                break;
            case R.id.image_restrict:
//                declineJob();
                showDeclineJobOfferDialog();

                break;
            case R.id.eta_request_lay:
                if (!isItFromHistory) {
                    if (mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(JOB_STATUS_ASSIGNED) || mCurrentJob.getCurrentStatus().getStatusCode().equalsIgnoreCase(JOB_STATUS_EN_ROUTE)) {
                        if (mCurrentJob.getEtaChangeHistory() != null && mCurrentJob.getEtaChangeHistory().size() > 0) {
                            displayPendingStatus(getString(R.string.pending_approval));
                        }
                    }
                }
                break;
            case R.id.text_customer_description:
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.CALL_CUSTOMER;
                if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getCustomerCallbackNumber()) && isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
//                    mHomeActivity.mintlogEvent("Calling customer - " + mCurrentJob.getCustomerCallbackNumber());
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("phone_number", mCurrentJob.getCustomerCallbackNumber());
                    extraDatas.put("dispatch_id", mCurrentJob.getDispatchId());
                    mHomeActivity.mintlogEventExtraData("Calling Customer", extraDatas);

                    if (Utils.isCallSupported(mHomeActivity)) {
                        try {
                            makePhoneCall(mCurrentJob.getCustomerCallbackNumber());
                        } catch (Exception e) {
                            mUserError.message = "Cannot make call";
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                        }
                    } else {
                        mUserError.message = "Cannot make call";
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                    }

                }
                break;
            case R.id.text_tow_destination_get_contact_number:
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.CALL_CUSTOMER;
                if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getTowLocation().getPhoneNumber().toString())) {
//                    mHomeActivity.mintlogEvent("Calling customer - " + mCurrentJob.getCustomerCallbackNumber());
                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("phone_number", mCurrentJob.getTowLocation().getPhoneNumber().toString());
                    extraDatas.put("dispatch_id", mCurrentJob.getDispatchId());
                    mHomeActivity.mintlogEventExtraData("Calling Tow Destination Number", extraDatas);

                    if (Utils.isCallSupported(mHomeActivity)) {
                        try {
                            makePhoneCall(mCurrentJob.getTowLocation().getPhoneNumber().toString());
                        } catch (Exception e) {
                            mUserError.message = "Cannot make call";
                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                        }
                    } else {
                        mUserError.message = "Cannot make call";
                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                    }

                }
                break;
        }
        mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
    }

    private void showDeclineJobOfferDialog() {

        FragmentManager manager = getFragmentManager();
        DeclineJobOfferFragment mydialog = DeclineJobOfferFragment.newInstance();
        mydialog.setTargetFragment(BaseJobDetailsFragment.this, NccConstants.DISPATCHER_DECLINE_JOB);
        mydialog.show(manager, "mydialog");

    }

    public void showGeofenceArray() {
        if (SparkLocationUpdateService.jobDataForGeofenceArrayList != null && SparkLocationUpdateService.jobDataForGeofenceArrayList.size() > 0) {
            String jobIds = "<font color='red'>" + SparkLocationUpdateService.jobDataForGeofenceArrayList.get(0).getDispatchId() + "</font>";
            for (int i = 1; i <
                    SparkLocationUpdateService.jobDataForGeofenceArrayList.size(); i++) {
                jobIds += "<br><br>" + SparkLocationUpdateService.jobDataForGeofenceArrayList.get(i).getDispatchId();
            }
            showDialog(jobIds);
        } else {
            if (mHomeActivity != null && !mHomeActivity.isFinishing()) {
                Toast.makeText(mHomeActivity, "Geofence List is Empty", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showDialog(String htmlText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView msg = new TextView(mHomeActivity);
        msg.setPadding(100, 50, 0, 0);
        msg.setGravity(Gravity.CENTER_VERTICAL);
        msg.setText(Html.fromHtml(htmlText));
        builder.setTitle("Geofence Job List");
        builder.setView(msg);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.create().show();
    }

    private void showETAAcceptJobOfferDialog() {

        FragmentManager manager = getFragmentManager();
        EtaApprovalDialogFragment mydialog = EtaApprovalDialogFragment.newInstance();
        mydialog.setTargetFragment(BaseJobDetailsFragment.this, NccConstants.DISPATCHER_REQUEST_JOB);
        mydialog.show(manager, "mydialog");

    }

    public void checkEtaValue(int mEtaValue) {
        mTextSetEta.setText(
                mEtaValue + " " + getResources().getString(R.string.alert_update_eta_min));
        if (mEtaValue < 1) {
            mImageEtaDecrease.setClickable(false);
            mImageEtaDecrease.setEnabled(false);
            mImageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus_grey);
            mButtonRequest.setEnabled(false);
            mImageEtaIncrease.setEnabled(true);
            mImageEtaIncrease.setImageResource(R.drawable.ic_job_detail_add);
        } else {
            mImageEtaDecrease.setClickable(true);
            mImageEtaDecrease.setEnabled(true);
            mImageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus);
            mButtonRequest.setEnabled(true);
            mButtonRequest.setTextColor(getResources().getColor(R.color.ncc_white));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && isAdded()) {
            if (requestCode == NccConstants.DISPATCHER_DECLINE_JOB) {
                String reasonCode = data.getStringExtra("reasonCode");
                String selectedString = data.getStringExtra("data");

                HashMap<String, String> extraData = new HashMap<>();
                extraData.put("ReasonCode", reasonCode);
                extraData.put("selectedString", selectedString);

                mHomeActivity.mintlogEventExtraData("Declining Job: " + mDispatchNumber, extraData);
                //updateStatus(NccConstants.JOB_STATUS_DECLINED, reasonCode, null);
                updateDeclineStatus(mHomeActivity, mAcceptDecline, mCurrentJob, mDispatchNumber, mUserError, reasonCode, selectedString);

            } else if (requestCode == NccConstants.DISPATCHER_REQUEST_JOB) {
                int position = data.getIntExtra("position", 0);
                String reportCode = getResources().getStringArray(R.array.array_eta_approval_code)[position];

                mHomeActivity.mintlogEvent("Accepting Job: " + mDispatchNumber);
                //updateStatus(NccConstants.JOB_STATUS_ACCEPTED, reportCode, null);
                updateAcceptStatus(mHomeActivity, mAcceptDecline, mCurrentJob, mDispatchNumber, mUserError, mEtaValue, currentStatusForUpdate, reportCode);

            }

        }
    }

    @Override
    public void onChoiceClicked(int position, int options) {

    }

    public boolean isActiveJob(String currentStatusCode) {
        if (!TextUtils.isEmpty(currentStatusCode) && (JOB_STATUS_ASSIGNED.equalsIgnoreCase(currentStatusCode) || JOB_STATUS_EN_ROUTE.equalsIgnoreCase(currentStatusCode) ||
                JOB_STATUS_ON_SCENE.equalsIgnoreCase(currentStatusCode) || JOB_STATUS_TOWING.equalsIgnoreCase(currentStatusCode) ||
                JOB_STATUS_DESTINATION_ARRIVAL.equalsIgnoreCase(currentStatusCode))) {

            return true;
        }
        return false;
    }

    private void toggleAcceptDeclineEtaLayout(boolean isEnable) {
        if (mCurrentJob.getOfferedToFacilitiesCount() == 1) {
//                            mTextJobOffer.setText("Exclusive Offer");
            mImageExclusiveOffer.setImageResource(R.drawable.ic_offer_exclusive);
            mTextEtaDescription.setTextColor(getContext().getResources().getColor(R.color.ncc_black));
        } else if (mCurrentJob.getOfferedToFacilitiesCount() > 1) {
//                            mTextJobOffer.setText(
//                                    "Offered to " + mCurrentJob.getOfferedToFacilitiesCount() + " partners");
            mImageExclusiveOffer.setImageResource(R.drawable.ic_offer_nonexclusive);
            mTextEtaDescription.setTextColor(getContext().getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
        }
        if (mCurrentJob.getOfferAcceptedByFacilitiesCount() > 0) {

            if (mCurrentJob.getOfferAcceptedByFacilitiesCount() == 1
                    && mCurrentJob.getOfferedToFacilitiesCount() == 1) {
//                                mTextJobOffer.setText("Offer requested");
                mImageExclusiveOffer.setImageResource(R.drawable.ic_offer_nonexclusive);
                mTextEtaDescription.setTextColor(getContext().getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
            } else {
//                                mTextJobOffer.setText(mCurrentJob.getOfferAcceptedByFacilitiesCount()
//                                        + " of "
//                                        + mCurrentJob.getOfferedToFacilitiesCount()
//                                        + " partners requested");
                mImageExclusiveOffer.setImageResource(R.drawable.ic_offer_nonexclusive);
                mTextEtaDescription.setTextColor(getContext().getResources().getColor(R.color.jobdetail_status_info_cancel_text_green));
            }
        }

        if (!TextUtils.isEmpty(mCurrentJob.getDispatchCreatedDate())) {
            long startTime =
                    getTimeDifference(mCurrentJob.getDispatchCreatedDate().toString());

            if (NccConstants.JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())) {
                if (startTime < JOB_DISPLAY_TIME_LIMIT_SEC) {
                    mTimerDisposable = Observable.intervalRange(startTime,
                            JOB_DISPLAY_TIME_LIMIT_SEC - startTime, 0, 1,
                            TimeUnit.SECONDS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnNext(aLong -> {

                                long sec = aLong % 60;
                                long min = (aLong / 60) % 60;

                                mTextEtaDescription.setText(
                                        String.format("%02d:%02d", min, sec));
                            })
                            .doOnComplete(() -> {
                                mTimerDisposable.dispose();
                                mTextEtaDescription.setText("60:00");
                            })
                            .subscribe();
                } else {
                    mTextEtaDescription.setText("60:00");
                }
            } else {
                long sec = startTime % 60;
                long min = (startTime / 60) % 60;
                mTextEtaDescription.setText(
                        String.format("%02d:%02d", min, sec));
            }
        } else {
            mTextEtaDescription.setText("60:00");
        }

        if (isEnable) {

            mButtonRequest.setBackgroundColor(getResources().getColor(R.color.ncc_blue));
            mButtonRequest.setClickable(true);
            mButtonRequest.setEnabled(true);
            mButtonRequest.setTextColor(getResources().getColor(R.color.ncc_white));

            mImageRestrict.setClickable(true);
            mImageRestrict.setEnabled(true);
            mImageRestrict.setImageResource(R.drawable.ic_job_detail_restricted);

            mImageEtaDecrease.setClickable(true);
            mImageEtaDecrease.setEnabled(true);
            mImageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus);

            mImageEtaIncrease.setClickable(true);
            mImageEtaIncrease.setEnabled(true);
            mImageEtaIncrease.setImageResource(R.drawable.ic_job_detail_add);

            textEta.setText("Set ETA");
            textEta.setTextColor(getResources().getColor(R.color.ncc_black));

            mTextSetEta.setTextColor(getContext().getResources().getColor(R.color.ncc_black));

        } else {
            mButtonRequest.setBackgroundColor(getResources().getColor(R.color.ncc_background));
            mButtonRequest.setClickable(false);
            mButtonRequest.setEnabled(false);
            mButtonRequest.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));

            mImageRestrict.setClickable(false);
            mImageRestrict.setEnabled(false);
            mImageRestrict.setImageResource(R.drawable.ic_job_detail_restricted_grey);

            mImageEtaDecrease.setClickable(false);
            mImageEtaDecrease.setEnabled(false);
            mImageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus_grey);

            mImageEtaIncrease.setClickable(false);
            mImageEtaIncrease.setEnabled(false);
            mImageEtaIncrease.setImageResource(R.drawable.ic_job_detail_add_grey);

            textEta.setText(Utils.getDisplayStatusText(getActivity(), mCurrentJob.getCurrentStatus().getStatusCode()) + "...");
            textEta.setTextColor(getResources().getColor(R.color.jobdetail_pending_color));

            mTextEtaDescription.setTextColor(getContext().getResources().getColor(R.color.ncc_text_disabled_color));
            mTextSetEta.setTextColor(getContext().getResources().getColor(R.color.ncc_text_disabled_color));

            mTextEtaDescription.setVisibility(View.GONE);
            mImageExclusiveOffer.setVisibility(View.GONE);

        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mHomeActivity != null) {
            mHomeActivity.setCurrentThreadName("");
        }
        removeEventListners();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mHomeActivity != null) {
            mHomeActivity.setCurrentThreadName("");
        }
        removeEventListners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHomeActivity != null) {
            mHomeActivity.setCurrentThreadName("");
        }
        removeEventListners();
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void removeEventListners() {

        try {
            if (mTimerDisposable != null) {
                mTimerDisposable.dispose();
            }
            if (mProfileReference != null) {
                mProfileReference.removeEventListener(profileValueEventListener);
            }
            if (mFacilityReference != null) {
                mFacilityReference.removeEventListener(mFacilityChangeEventListner);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCustomerInfo() {
        if (Utils.isNetworkAvailable()) {
            mTextCustomerDescription.setVisibility(View.INVISIBLE);
            mCustomerNameProgressBarLayout.setVisibility(View.VISIBLE);
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        nccApi.getCustomerInfo(BuildConfig.CUSTOMERINFO_URL + mCurrentJob.getDispatchTraceId() + "/client/" + mCurrentJob.getClientId(), "Bearer " + mPrefs.getString(NccConstants.APIGEE_TOKEN, ""))
                                .enqueue(new Callback<CustomerInfoModel>() {
                                    @Override
                                    public void onResponse(Call<CustomerInfoModel> call, Response<CustomerInfoModel> response) {
                                        if (isAdded()) {
                                            mTextCustomerDescription.setVisibility(View.VISIBLE);
                                            mCustomerNameProgressBarLayout.setVisibility(View.GONE);
                                            mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_black));
                                            mTextCustomerDescription.setText(getString(R.string.not_available));
                                            if (response.body() != null) {
                                                CustomerInfoModel customerInfo = response.body();
                                                if (customerInfo != null && customerInfo.getCallerDetails() != null && !TextUtils.isEmpty(customerInfo.getCallerDetails().getCallerFirstName())) {

                                                    HashMap<String, String> extraDatas = new HashMap<>();
                                                    extraDatas.put("json", new Gson().toJson(customerInfo));
                                                    extraDatas.put("dispatch_id", mCurrentJob.getDispatchId());
                                                    mHomeActivity.mintlogEventExtraData("Customer Info Response", extraDatas);

                                                    mCustomerName = customerInfo.getCallerDetails().getCallerFirstName() + " " + customerInfo.getCallerDetails().getCallerLastName();
                                                    setCustomerName(mCustomerName);
                                                } else if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getCustomerCallbackNumber())) {
//                                                    mTextCustomerDescription.setText(mCurrentJob.getCustomerCallbackNumber());
                                                    mCustomerName = mCurrentJob.getCustomerCallbackNumber();
                                                    setCustomerName(mCustomerName);
                                                }

                                            } else if (response.errorBody() != null) {
                                                try {
                                                    JSONObject errorResponse = new JSONObject(response.errorBody().string());
                                                    if (errorResponse != null) {
                                                        if (mHomeActivity != null && !mHomeActivity.isFinishing() && errorResponse.get("message") != null && errorResponse.get("message").toString().length() > 1 && isAdded()) {
                                                            Toast.makeText(mHomeActivity, (String) errorResponse.get("message"), Toast.LENGTH_LONG).show();
                                                        }
                                                        if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getCustomerCallbackNumber())) {
                                                            mCustomerName = mCurrentJob.getCustomerCallbackNumber();
                                                            setCustomerName(mCustomerName);
                                                        } else {
                                                            mTextCustomerDescription.setText(getString(R.string.not_available));
                                                        }
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }
                                                if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getCustomerCallbackNumber())) {
                                                    mCustomerName = mCurrentJob.getCustomerCallbackNumber();
                                                    setCustomerName(mCustomerName);
                                                } else {
                                                    mTextCustomerDescription.setText(getString(R.string.not_available));
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CustomerInfoModel> call, Throwable t) {
                                        if (isAdded()) {
                                            mTextCustomerDescription.setVisibility(View.VISIBLE);
                                            mCustomerNameProgressBarLayout.setVisibility(View.GONE);
                                            mHomeActivity.mintlogEvent("Customer Info Api Error - " + t.getLocalizedMessage());
                                            Toast.makeText(mHomeActivity, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                            if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getCustomerCallbackNumber())) {
                                                mCustomerName = mCurrentJob.getCustomerCallbackNumber();
                                                setCustomerName(mCustomerName);
                                            } else {
                                                mTextCustomerDescription.setText(getString(R.string.not_available));
                                            }
                                        }
                                    }
                                });
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getCustomerCallbackNumber())) {
                mCustomerName = mCurrentJob.getCustomerCallbackNumber();
                setCustomerName(mCustomerName);
            } else {
                mTextCustomerDescription.setText(getString(R.string.not_available));
            }
        }
    }

    private void setCustomerName(String mCustomerName) {
        if (isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode())) {
            mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_blue));
            mTextCustomerDescription.setEnabled(true);
        } else {
            mTextCustomerDescription.setTextColor(getResources().getColor(R.color.ncc_black));
            mTextCustomerDescription.setEnabled(false);
        }
        mTextCustomerDescription.setText(Utils.toCamelCase(mCustomerName));
    }

    protected void updateStatus(String statusCode, String reasonCode, String userAction) {
        updateStatus(statusCode, reasonCode, userAction, null);
        //updateStatusAPI(statusCode, reasonCode, userAction, null);
    }

    //For CANCEL/UNSUCCESSFUL
    protected void updateStatus(String statusCode, String reasonCode, String userAction, String statusChangeRequestedBy) {
        if (Utils.isNetworkAvailable()) {
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        currentStatusForUpdate = mCurrentJob.getCurrentStatus();
                        statusHistoryForUpdate = mCurrentJob.getStatusHistory();
                        if (statusHistoryForUpdate == null) {
                            statusHistoryForUpdate = new ArrayList<>();
                        }
                        if (currentStatusForUpdate != null) {
                            statusHistoryForUpdate.add(currentStatusForUpdate);
                            currentStatusForUpdate = new Status();
                            currentStatusForUpdate.setAppId(NccConstants.SUB_SOURCE);

                            currentStatusForUpdate.setStatusCode(statusCode);

                            currentStatusForUpdate.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                            currentStatusForUpdate.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                            currentStatusForUpdate.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                            currentStatusForUpdate.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                            currentStatusForUpdate.setSource("FIREBASE");
                            currentStatusForUpdate.setSubSource(NccConstants.SUB_SOURCE);
                            currentStatusForUpdate.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                            if (!TextUtils.isEmpty(reasonCode)) {
                                currentStatusForUpdate.setReasonCode(reasonCode);
                            }

                            if (!TextUtils.isEmpty(userAction)) {
                                currentStatusForUpdate.setUserAction(userAction);
                            }
                            if (!TextUtils.isEmpty(statusChangeRequestedBy)) {
                                currentStatusForUpdate.setStatusChangeRequestedBy(statusChangeRequestedBy);
                            }

                            currentStatusForUpdate.setIsPending(true);

                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("current_status", new Gson().toJson(currentStatusForUpdate));
                            extraDatas.put("dispatch_id", mDispatchNumber);
                            ((HomeActivity) getActivity()).mintlogEventExtraData("Update Status: " + statusCode, extraDatas);

                            myRef.runTransaction(new Transaction.Handler() {
                                @Override
                                public Transaction.Result doTransaction(MutableData mutableData) {
                                    if (!(currentStatusForUpdate.getStatusCode().equalsIgnoreCase(mutableData.getValue(Status.class).getStatusCode()))) {

                                        HashMap<String, String> extraDatas = new HashMap<>();
                                        extraDatas.put("current_status", new Gson().toJson(currentStatusForUpdate));
                                        extraDatas.put("dispatch_id", mDispatchNumber);
                                        ((HomeActivity) getActivity()).mintlogEventExtraData("Update Status: " + statusCode, extraDatas);

                                        if (statusCode.equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)) {
                                            mCurrentJob.setTotalEtaInMinutes(mEtaValue);
                                        }

                                        mCurrentJob.setStatusHistory(statusHistoryForUpdate);
                                        mCurrentJob.setCurrentStatus(currentStatusForUpdate);
                                        mutableData.setValue(mCurrentJob);
                                        return Transaction.success(mutableData);
                                    } else {
                                        return Transaction.abort();
                                    }
                                }

                                @Override
                                public void onComplete(DatabaseError databaseError, boolean b,
                                                       DataSnapshot dataSnapshot) {
                                    if (isAdded() && !mHomeActivity.isFinishing() && !getResources().getBoolean(R.bool.isTablet)) {
                                        mHomeActivity.popAllBackstack();
                                        mHomeActivity.mBottomBar.setCurrentItem(0);
                                    }
                                }
                            });
                        }
                    }
                }

                @Override
                public void onRefreshFailure() {
//                    hideProgress();
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    interface IAcceptDecline {
        void enableAcceptButton(boolean state);

        void enableDeclineButton(boolean state);
    }


}
