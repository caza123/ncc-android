package com.agero.ncc.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;

/**
 * Adapter for HistoryFragment.
 */
public class HistoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements StickyHeaderAdapter<HistoryListAdapter.HeaderHolder> {
    private Context mContext;
    private ArrayList<JobDetail> mHistoryList;
    private ArrayList<String> mHeaderDate;
    private int mSelectedJobPosition;
    private int mOldHeaderId = 0;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;


    public HistoryListAdapter(Context mContext, ArrayList<JobDetail> mHistoryList, ArrayList<String> headerDate, int selectedJobPosition) {
        this.mContext = mContext;
        this.mHistoryList = mHistoryList;
        this.mHeaderDate = headerDate;
        this.mSelectedJobPosition = selectedJobPosition;
    }

    public void clear() {
        if (mHistoryList != null) {
            mHistoryList.clear();
        }
        if (mHeaderDate != null) {
            mHeaderDate.clear();
        }
        mSelectedJobPosition = 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.history_item, parent, false));
        }else{
            return new FooterViewHolder(LayoutInflater.from(mContext).inflate(R.layout.history_job_list_footer, parent, false));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if(position < mHistoryList.size()) {
                ItemViewHolder holder = (ItemViewHolder) viewHolder;

                JobDetail job = mHistoryList.get(holder.getAdapterPosition());
//        if (mHistoryList.size() - 1 == position) {
//            holder.mViewHistoryBorder.setVisibility(View.INVISIBLE);
//        }else{
//            holder.mViewHistoryBorder.setVisibility(View.VISIBLE);
//        }
                if (mContext.getResources().getBoolean(R.bool.isTablet)) {
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mSelectedJobPosition = position;
                            notifyDataSetChanged();
                        }
                    });

                    if (mSelectedJobPosition == holder.getAdapterPosition()) {
                        holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.tab_tap_color));
                    } else {
                        holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.ncc_white));
                    }
                }
                try {
                    if (job != null && job.getDispatchId() != null) {
                        holder.mJobId.setText(UiUtils.getJobIdDisplayFormat(job.getDispatchId()));
                    }
                    holder.mTextHistoryTow.setText(Utils.getDisplayServiceTypes(mContext, job.getServiceTypeCode()) + " - " + job.getVehicle().getYear()
                            + " " + job.getVehicle().getMake() + " " + job.getVehicle().getModel() + " " + job.getVehicle().getColor());
//            if(Utils.isTow(job.getServiceTypeCode())){
//                holder.mTextAddressHistory.setText(Utils.toCamelCase(job.getTowLocation().getAddressLine1() + ", "
//                        + job.getTowLocation().getCity() + ", " )+ job.getTowLocation().getState() + " "+job.getTowLocation().getPostalCode());
//            }else{
                    holder.mTextAddressHistory.setText(Utils.toCamelCase(job.getDisablementLocation().getAddressLine1() + ", "
                            + job.getDisablementLocation().getCity() + ", ") + job.getDisablementLocation().getState() + " " + job.getDisablementLocation().getPostalCode());
//            }

                    try {
                        if (job.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_JOB_COMPLETED)) {
                            holder.mTextHistoryEta.setText(mContext.getResources().getString(R.string.history_text_completed) + " " + mContext.getString(R.string.dot) + " "
                                    + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(job.getDispatchCreatedDate()), job.getDisablementLocation().getTimeZone()));
                        } else {

                            if (job.getCurrentStatus().getIsPending() != null && job.getCurrentStatus().getIsPending() && !job.getCurrentStatus().getStatusCode().equalsIgnoreCase(NccConstants.JOB_STATUS_ACCEPTED)) {
                                holder.mTextHistoryEta.setText(mContext.getString(R.string.text_pending) + " " + Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode()) + "... " + mContext.getString(R.string.dot) + " "
                                        + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(job.getDispatchCreatedDate()), job.getDisablementLocation().getTimeZone()));
//                        holder.mTextHistoryEta.setTextColor(mContext.getResources()
//                                .getColor(R.color.jobdetail_pending_color));
                            } else {
                                holder.mTextHistoryEta.setText(Utils.getDisplayStatusText(mContext, job.getCurrentStatus().getStatusCode()) + " " + mContext.getString(R.string.dot) + " "
                                        + DateTimeUtils.getDisplayTimeForHistory(DateTimeUtils.parse(job.getDispatchCreatedDate()), job.getDisablementLocation().getTimeZone()));
//                        holder.mTextHistoryEta.setTextColor(mContext.getResources()
//                                .getColor(R.color.jobdetail_heading_color));
                            }


                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    public int getItemCount() {
        return mHistoryList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
      if (position == mHistoryList.size()) {
            return TYPE_FOOTER;
        }else {
          return TYPE_ITEM;
      }
    }

    @Override
    public long getHeaderId(int position) {
        String time = null;
        try {

            if(mHistoryList.size() > position) {
                time = DateTimeUtils.getDisplayForDate(mHistoryList.get(position).getDisablementLocation().getTimeZone(),DateTimeUtils.parse(mHistoryList.get(position).getDispatchCreatedDate()));
                if (time != null && mHeaderDate.size() > 0 && mHeaderDate.contains(time)) {
                    mOldHeaderId = mHeaderDate.indexOf(time);
                    return mHeaderDate.indexOf(time);
                } else {
                    return mOldHeaderId;
                }
            }else{
                return mOldHeaderId;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new HeaderHolder(LayoutInflater.from(mContext).inflate(R.layout.history_header, parent, false));
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder viewholder, int position) {
        if (mHeaderDate != null && position < mHistoryList.size()) {
            int headerId = (int) getHeaderId(position);
            if(mHeaderDate.size() > headerId) {
                if (DateTimeUtils.isToday(mHeaderDate.get(headerId))) {
                    viewholder.mTextHistoryHeader.setText(DateTimeUtils.getDisplayTimeForHistory(mHeaderDate.get(headerId)));
                } else {
                    viewholder.mTextHistoryHeader.setText(DateTimeUtils.getDisplayOldTimeForHistory(mHeaderDate.get(headerId)));
                }
            }
        }
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_history_tow)
        TextView mTextHistoryTow;
        @BindView(R.id.text_address_history)
        TextView mTextAddressHistory;
        @BindView(R.id.text_history_eta)
        TextView mTextHistoryEta;
        @BindView(R.id.view_history_border)
        View mViewHistoryBorder;
        @BindView(R.id.text_active_job_id)
        TextView mJobId;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class FooterViewHolder extends RecyclerView.ViewHolder {

        FooterViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_history_header)
        TextView mTextHistoryHeader;

        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
