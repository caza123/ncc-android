package com.agero.ncc.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.model.Equipment;
import com.agero.ncc.model.Profile;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import de.hdodenhof.circleimageview.CircleImageView;

public class CompanyContactsAdapter extends RecyclerView.Adapter<CompanyContactsAdapter.MyHolder> implements StickyHeaderAdapter<CompanyContactsAdapter.HeaderHolder> {
    private Context mContext;
    private ArrayList<Profile> mContactsList;
    private ChildItemClick mChildItemClick;
    private int[] mChildCount = new int[3];

    public CompanyContactsAdapter(Context context, ArrayList<Profile> contactsList, ChildItemClick childItemClick) {
        this.mContext = context;
        this.mContactsList = contactsList;
        this.mChildItemClick = childItemClick;
        mChildCount[0] = 0;
        mChildCount[1] = 0;
        mChildCount[2] = 0;
        for (Profile profile :
                contactsList) {
            ArrayList<String> roles = (ArrayList<String>) profile.getRoles();
            if (roles == null) {
                mChildCount[0]++;
            } else if (roles.size() == 1 && roles.contains(NccConstants.USER_ROLE_DRIVER)) {
                mChildCount[2]++;
            } else {
                mChildCount[1]++;
            }
        }

    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.company_contact_child_item, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {

        List<String> roles = mContactsList.get(position).getRoles();

        if (roles != null) {
            String contactImageUrl = mContactsList.get(position).getImageUrl();
            String name = "";
            String userName = "";
            if (!TextUtils.isEmpty(mContactsList.get(position).getFirstName())) {
                userName += mContactsList.get(position).getFirstName()+ " ";
                name += mContactsList.get(position).getFirstName().charAt(0);
            }
            if (!TextUtils.isEmpty(mContactsList.get(position).getLastName())) {
                userName += mContactsList.get(position).getLastName();
                name += mContactsList.get(position).getLastName().charAt(0);
            }
            if (!TextUtils.isEmpty(name)) {
                holder.mImageProfileTv.setText(name.toUpperCase());
            } else {
                holder.mImageProfileTv.setText("");
            }
            if (!TextUtils.isEmpty(userName)) {
                holder.mPersonName.setText(userName.trim());
            } else {
                holder.mImageProfileTv.setText("");
            }


            holder.mImageProfileTv.setVisibility(View.VISIBLE);

            if (contactImageUrl != null && !contactImageUrl.isEmpty()) {

                Glide.with(mContext).load(contactImageUrl).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model,
                                                   Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.mImageProfileTv.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.mImageProfile);
            }
            if (getHeaderId(position) == 2) {
                if (mContactsList.get(position).getOnDuty() != null && mContactsList.get(position).getOnDuty()) {
                    Equipment equipment = mContactsList.get(position).getEquipment();
                    if (equipment != null && !"-1".equalsIgnoreCase(equipment.getEquipmentId())) {
                        holder.mRole.setText(equipment.getPlateState()
                                + "-" + equipment.getPlate() + " "
                                + mContext.getString(R.string.dot) + " " + equipment.getEquipmentType());
                    } else {
                        holder.mRole.setText(mContext.getString(R.string.toolbar_noequipment));
                    }
                    holder.mImageOnlineOffline.setImageResource(R.drawable.ic_online_dot);
                } else {
                    holder.mRole.setText(mContext.getString(R.string.title_offduty));
                    holder.mImageOnlineOffline.setImageResource(R.drawable.ic_offline_dot);
                }
            } else {
                holder.mRole.setText(Utils.getRoleText(mContext, mContactsList.get(position).getRoles()));
            }
        } else {
            holder.mImageProfile.setImageResource(R.drawable.ic_company_home);
            holder.mPersonName.setText(mContactsList.get(position).getFirstName());
            holder.mRole.setText(mContactsList.get(position).getLastName());
        }


        holder.mImagePhone.setTag(mContactsList.get(position).getMobilePhoneNumber());
        holder.mImageEmail.setTag(mContactsList.get(position).getEmail());
        holder.mImageMore.setTag(mContactsList.get(position));

        holder.mImageMore.setVisibility(View.VISIBLE);
        holder.mImagePhone.setVisibility(View.VISIBLE);
        if (getHeaderId(position) == 2) {
            holder.mImagePhone.setVisibility(View.GONE);
        } else {
            holder.mImageMore.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(mContactsList.get(position).getMobilePhoneNumber())) {
            holder.mImagePhone.setAlpha(0.4f);
            holder.mImagePhone.setEnabled(false);
        } else {
            holder.mImagePhone.setAlpha(1f);
            holder.mImagePhone.setEnabled(true);
        }

        if (position == mChildCount[0] - 1 || position == (mChildCount[0] + mChildCount[1] - 1) || position == (mChildCount[0] + mChildCount[1] + mChildCount[2] - 1)) {
            holder.mViewBorderLine.setVisibility(View.GONE);
            holder.mRole.setPadding(0, 0, 0, 32);
        } else {
            holder.mViewBorderLine.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return mContactsList.size();
    }

    @Override
    public long getHeaderId(int position) {
        ArrayList<String> roles = (ArrayList<String>) mContactsList.get(position).getRoles();
        if (roles == null) {
            return 0;
        } else if (roles.size() == 1 && roles.contains(NccConstants.USER_ROLE_DRIVER)) {
            return 2;
        } else {
            return 1;
        }
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return new HeaderHolder(LayoutInflater.from(mContext).inflate(R.layout.history_header, parent, false));
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder viewholder, int position) {
        int headerPosition = (int) getHeaderId(position);

        viewholder.mTextHistoryHeader.setText(NccConstants.COMPANY_CONTACTS_HEADERS[headerPosition]);
    }

    static class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_history_header)
        TextView mTextHistoryHeader;

        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class MyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_profile)
        CircleImageView mImageProfile;
        @BindView(R.id.image_profile_tv)
        TextView mImageProfileTv;
        @BindView(R.id.text_name)
        TextView mPersonName;
        @BindView(R.id.text_role)
        TextView mRole;
        @BindView(R.id.image_email)
        ImageView mImageEmail;
        @BindView(R.id.image_phone)
        ImageView mImagePhone;
        @BindView(R.id.image_more)
        ImageView mImageMore;
        @BindView(R.id.image_online_offline)
        ImageView mImageOnlineOffline;
        @BindView(R.id.view_border_contact_details)
        View mViewBorderLine;

        public MyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick({R.id.image_phone, R.id.image_email, R.id.image_more})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.image_phone:
                    String phoneNumber = (String) view.getTag();
                    mChildItemClick.phoneNumberClicked(phoneNumber);
                    break;
                case R.id.image_email:
                    String email = (String) view.getTag();
                    mChildItemClick.emailClicked(email);
                    break;
                case R.id.image_more:
                    Profile clickedProfile = (Profile) view.getTag();
                    int[] location = new int[2];
                    view.getLocationOnScreen(location);
                    mChildItemClick.moreClicked(clickedProfile, location);
                    break;
            }
        }
    }

    public interface ChildItemClick {
        void phoneNumberClicked(String phoneNumber);

        void emailClicked(String email);

        void moreClicked(Profile clickedProfile, int[] location);
    }
}
